//
//  ServerCalls.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 24/03/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "ServerCalls.h"

static ServerCalls *singletonInstance;

@implementation ServerCalls
- (id)init
{
    self = [super init];
    if (self) {
        
        appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
        
    }
    return self;
}
+ (ServerCalls *)getSingletonInstance
{
    if (!singletonInstance)
    {
        singletonInstance = [[ServerCalls alloc] init];
    }
    return singletonInstance;
}

-(NSData*)getServerResponseByMethod:(NSMutableData *)postbody andAppendRequestURL:(NSString *)appendString andRequestType:(NSString * )requestType
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSString *reponseStr;
    NSMutableURLRequest *request=(NSMutableURLRequest*)[NSMutableURLRequest requestWithURL:nil cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:600.0];
      NSString *tempUrlString2 =[[NSString stringWithFormat:@"%@%@",appDelegate.mainUrlString,appendString] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ;
//    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
//    http://cuztomise:123456@184.106.222.195/cuztomise_api/branches/pharma_api/mobileapp/Loginapp/format/json?&time=23:53:02&dbname=cuztomise_global&pin=11111&deviceid=123456&key=JVP8xGk4hsX2cZd0L3NQwYbI0mf4exPiSoAhVYnz&lat=23.987798798&long=-72.8743827
//    http://184.106.222.195/cuztomise_api/branches/pharma_api/mobileapp/Loginapp/format/json?&time=23:53:02&dbname=cuztomise_global&pin=11111&deviceid=12345&key=JVP8xGk4hsX2cZd0L3NQwYbI0mf4exPiSoAhVYnz&lat=23.987798798&long=-72.8743827
    [request setURL:[NSURL URLWithString:tempUrlString2]];
//    [request addValue:aClientId forHTTPHeaderField:@"Clientid"];
//    [request addValue:[Utils getValueFronUserDefault:aDeviceId] forHTTPHeaderField:@"Deviceid"];
//    [request addValue:@"iPhone" forHTTPHeaderField:@"client"];
//    [request addValue:dataLength forHTTPHeaderField:@"Content-Length"];
//    [request addValue:[Utils getValueFronUserDefault:aSessionToken] forHTTPHeaderField:@"Sessiontoken"];
    [ request setHTTPMethod: requestType ];
    [ request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSLog(@"request===%@",[request allHTTPHeaderFields]);
    NSLog(@"URL=====%@",[request URL]);
    NSLog(@"request===%@",request);
    NSError *theError = nil;
    NSData *returnData;
    NSURLResponse *urlResponse = nil;
    if ([self checkInternet])
    {
        returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&theError];
        
        if(theError==nil)
        {
            reponseStr= [[NSString alloc] initWithData:returnData encoding:NSASCIIStringEncoding];
            DebugLog(@"reponseStr %@",reponseStr);
            NSLog(@"reponseStr %@",reponseStr);
            return returnData;
            
        }
        return nil;
        
    }
    else
    {
        return nil;
    }
}
- (BOOL) connectedToNetwork
{
    Reachability *r = [Reachability reachabilityForInternetConnection];//[Reachability reachabilityWithHostName:@"kumoteamdemo.pagodabox.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    BOOL internet;
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
    {
        internet = NO;
    } else {
        internet = YES;
    }
    return internet;
}

-(BOOL) checkInternet
{
    //Make sure we have internet connectivity
    if([self connectedToNetwork] != YES)
    {
        return NO;
    }
    else {
        return YES;
    }
}


@end
