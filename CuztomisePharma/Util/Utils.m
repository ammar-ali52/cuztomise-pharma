//
//  Utils.m
//  Easyf6 Veterans
//
//  Created by Abhishek Batra on 27/01/15.
//  Copyright (c) 2015 Ammar. All rights reserved.
//

#import "Utils.h"

@implementation Utils
+(NSDate *)stringToDate:(NSString *) stringDate byFormatter: (NSString *) formatter
{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:formatter];
//    NSDate *tempDate = [dateFormatter dateFromString:stringDate];
//    dateFormatter = nil;
//
//    return tempDate;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:formatter];
    //    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    NSDate *tempDate = [dateFormatter dateFromString:stringDate];
    return tempDate;

}
+(NSString *) dateFromString: (NSDate *)stringDate byFormatter:(NSString *) formatter;
{
//    NSString *dateString = nil;
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:formatter];
//    dateString = [dateFormatter stringFromDate:stringDate];
//    
//    dateFormatter = nil;
//    return dateString;
    NSString *dateString = nil;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:formatter];
    //    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
//    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    dateString = [dateFormatter stringFromDate:stringDate];
    return dateString;
}

+(NSString *)getCurrentDateBySystemTimeZone
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setLocale:[NSLocale currentLocale]];
    
    return [dateFormat stringFromDate:date];
}


+(void)setValueToUserDefault:(NSString *)value andKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%@",value] forKey:key];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
+(NSString *)getValueFronUserDefault:(NSString *)key
{
    NSString * returnString;
    if ([[[NSUserDefaults  standardUserDefaults] valueForKey:key] length] > 0 && [[NSUserDefaults  standardUserDefaults] valueForKey:key]!=nil)
    {
        returnString = [[NSUserDefaults standardUserDefaults] valueForKey:key];
    }
    return returnString;
}
+(NSString *)getAppendURLForConsumer:(BOOL)value
{
    if (value)
        return @"/consumerService";
    
    else
        return @"/service/validateClient";
}
+ (void) deleteAllObjects: (NSString *) entityDescription  {
//    AppDelegate * appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:appDelegate.managedObjectContext];
//    [fetchRequest setEntity:entity];
//    
//    NSError *error;
//    NSArray *items = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
//    for (NSManagedObject *managedObject in items) {
//        [appDelegate.managedObjectContext deleteObject:managedObject];
//        DebugLog(@"%@ object deleted",entityDescription);
//    }
//    if (![appDelegate.managedObjectContext save:&error]) {
//         DebugLog(@"%@ Error deleting",entityDescription);
//    }
    
}
//+(NSDate*)dateFromString:(NSString*)dateStr
//{
//    LoginManager *login=[[LoginManager alloc] init];
//    CompanyInfo *company=[login getCompanyInfo];
//    [login release];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:company.timezone]];
//    //    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    
//    NSDate *dateFromString = [dateFormatter dateFromString:dateStr];
//    NSDate* sourceDate = dateFromString;
//    [dateFormatter release];
//    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithName:company.timezone];
//    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
//    
//    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
//    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
//    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
//    NSDate* destinationDate = [[[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate] autorelease];
//    
//    return destinationDate;
//    
//}

+(void)sendGeoLocationToServer
{
   
    
}
@end
