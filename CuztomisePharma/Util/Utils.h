//
//  Utils.h
//  Easyf6 Veterans
//
//  Created by Abhishek Batra on 27/01/15.
//  Copyright (c) 2015 Ammar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseImportClass.h"

@interface Utils : NSObject
+(NSDate *)stringToDate:(NSString *) stringDate byFormatter: (NSString *) formatter;
+(NSString *) dateFromString: (NSDate *)stringDate byFormatter:(NSString *) formatter;
+(NSString *)getCurrentDateBySystemTimeZone;

+(NSString *)getValueFronUserDefault:(NSString *)key;
+(void)setValueToUserDefault:(NSString *)value andKey:(NSString *)key;
+(NSString *)getAppendURLForConsumer:(BOOL)value;
+ (void) deleteAllObjects: (NSString *) entityDescription ;
+(void)sendGeoLocationToServer;

@end
