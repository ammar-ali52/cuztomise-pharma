//
//  Constant.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 03/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#ifndef CuztomisePharma_Constant_h
#define CuztomisePharma_Constant_h

#define aUserId                                         @"userID"
#define aSuccess                                        @"success"
#define aMsg                                            @"msg"
#define aServerKey                                      @"JVP8xGk4hsX2cZd0L3NQwYbI0mf4exPiSoAhVYnz"
#define aRadius                                         @"radius"
#define aFirstTimeSync                                  @"firsTimeSync"
#define aPushNotificationKey                            @"pns"
#define aCurrentLat                                     @"lat"
#define aCurrentLong                                    @"long"
#define aParsingError                                   @"Parsing Error"
#define aSuccessfull                                    @"Parse sucessfully"
#define aInternetError                                  @"Please check internet connection"
#define aDeviceId                                       @"DeviceId"

#define aSkipWorkOrder                                  40
#define aNewWorkOrder                                   10
#define aRescheduleWorkOrder                            30
#define aClosedWorkOrder                                50

#endif
