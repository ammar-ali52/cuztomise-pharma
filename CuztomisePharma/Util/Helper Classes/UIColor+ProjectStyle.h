//
//  UIColor+ProjectStyle.h
//  gfst
//
//  Created by Ammar Ali on 16/09/13.
//  Copyright (c) 2013 Gruma Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectStyle.h"
@interface UIColor (ProjectStyle)
+ (UIColor *)appBlackColor;
+ (UIColor *)appWhiteColor;
+ (UIColor *)appLightGrayColor;
+ (UIColor *)appGreenColor;
+ (UIColor *)appGrayColor;
+ (UIColor *)appDarkGrayColor;
+ (UIColor *)appClearColor;

@end
