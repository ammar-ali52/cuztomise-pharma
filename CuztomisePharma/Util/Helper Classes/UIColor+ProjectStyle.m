//
//  UIColor+ProjectStyle.m
//  gfst
//
//  Created by Ammar Ali on 16/09/13.
//  Copyright (c) 2013 Gruma Corporation. All rights reserved.
//

#import "UIColor+ProjectStyle.h"

@implementation UIColor (ProjectStyle)

+ (UIColor *)appBlackColor;
{
    return  [UIColor blackColor];
}
+ (UIColor *)appWhiteColor
{
    return  [UIColor whiteColor];
}
+ (UIColor *)appLightGrayColor
{
    return  [UIColor lightGrayColor];
}
+ (UIColor *)appGreenColor
{
   return [UIColor colorWithRed:136.0/255.0 green:164.0/255.0 blue:60.0/255.0 alpha:1.0];
}
+ (UIColor *)appGrayColor;
{
    return  [UIColor grayColor];
}
+ (UIColor *)appDarkGrayColor
{
    return  [UIColor darkGrayColor];
}
+ (UIColor *)appClearColor
{
    return [UIColor clearColor];
}


@end
