//
//  UIFont+ProjectStyle.h
//  gfst
//
//  Created by Ammar Ali on 16/09/13.
//  Copyright (c) 2013 Gruma Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectStyle.h"

@interface UIFont (ProjectStyle)

+ (UIFont *)appSmallFont;
+ (UIFont *)appMediumFont;
+ (UIFont *)appNormalFont;
+ (UIFont *)appLargeFont;
+ (UIFont *)appExtraLargeFont;
+ (UIFont *)appDoubleExtraLargeFont;



@end
