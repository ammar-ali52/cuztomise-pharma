//
//  UIFont+ProjectStyle.m
//  gfst
//
//  Created by Ammar Ali on 16/09/13.
//  Copyright (c) 2013 Gruma Corporation. All rights reserved.
//

#import "UIFont+ProjectStyle.h"

@implementation UIFont (ProjectStyle)

+ (UIFont *)appSmallFont
{
    return [UIFont fontWithName:@"Arial" size:10.0];
}
+ (UIFont *)appMediumFont
{
    return [UIFont fontWithName:@"Arial" size:12.0];
}
+ (UIFont *)appNormalFont
{
    return [UIFont fontWithName:@"Arial" size:14.0];
}
+ (UIFont *)appLargeFont;
{
    return [UIFont fontWithName:@"Arial" size:16.0];
}
+ (UIFont *)appExtraLargeFont
{
    return [UIFont fontWithName:@"Arial" size:18.0];
}
+ (UIFont *)appDoubleExtraLargeFont
{
    return [UIFont fontWithName:@"Arial" size:20.0];
}
@end
