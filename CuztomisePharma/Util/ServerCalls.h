//
//  ServerCalls.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 24/03/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BaseImportClass.h"
#import "AppDelegate.h"

@class AppDelegate;
@interface ServerCalls : NSObject
{
    AppDelegate * appDelegate;
}
+ (ServerCalls *)getSingletonInstance;

-(NSData*)getServerResponseByMethod:(NSMutableData *)postbody andAppendRequestURL:(NSString *)appendString andRequestType:(NSString * )requestType;
@end
