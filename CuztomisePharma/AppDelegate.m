//
//  AppDelegate.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 16/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize mainUrlString;
@synthesize apnsToken=_apnsToken;
@synthesize locationManager=_locationManager;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor appWhiteColor];
    
    self.rootViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    mainUrlString=@"http://52.11.119.181/cuztomise_api/branches/pharma_api/";
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.rootViewController];
//    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    self.window.rootViewController = self.navigationController;
    if (![Utils getValueFronUserDefault:aDeviceId])
    {
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        if([currSysVer floatValue]>6.0)
        {
            [Utils setValueToUserDefault:[[NSUUID UUID] UUIDString] andKey:aDeviceId];
        }
        else
        {
            [Utils setValueToUserDefault:(NSString*)CFBridgingRelease(CFUUIDCreateString(NULL, CFUUIDCreate(NULL))) andKey:aDeviceId];
        }
    }
    DebugLog(@"In app delegate");

    [self.window makeKeyAndVisible];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        
        
        NSLog(@"registerForPushNotification: For iOS >= 8.0");
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:
         [UIUserNotificationSettings settingsForTypes:
          (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge)
                                           categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        
    }
    else
    {
        NSLog(@"registerForPushNotification: For iOS < 8.0");
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
//         (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    }
    //    _timeSlot=[NSString stringWithFormat:@"5 min"];
    //    [ self StartThread];
    _locationManager = [[CLLocationManager alloc] init];
    if ([_locationManager respondsToSelector:
         @selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
    _locationManager.delegate=self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [_locationManager startUpdatingLocation];

//    if(threadTimer)
//    {
//        [threadTimer invalidate];
//        threadTimer = nil;
//    }
//    threadTimer=[NSTimer scheduledTimerWithTimeInterval:aSendLocation target:self selector:@selector(sendLocation) userInfo:nil repeats:YES];
//    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.incoders.CuztomisePharma" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"CuztomisePharma" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"CuztomisePharma.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    NSString* oldToken = nil;
    if ([[Utils getValueFronUserDefault:aPushNotificationKey] length] > 0)
    {
        oldToken = [Utils getValueFronUserDefault:aPushNotificationKey];
    }
    NSString* newToken = [deviceToken description];
    newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    [Utils setValueToUserDefault:newToken andKey:aPushNotificationKey];
    NSLog(@"My token is: %@", newToken);
    
    
    
    // If the token changed and we already sent the "join" request, we should
    // let the server know about the new device token.
    //  NSString *uniqueIdenitifier = [[UIDevice currentDevice] uniqueIdentifier];
    if (![newToken isEqualToString:oldToken])
    {
        
        
        
        
        //        _apnsToken=newToken;
        //        NSData * returnData;
        //        NSMutableData *postbody = [NSMutableData data];
        //        [postbody appendData:[[NSString stringWithFormat:@"&param=addIphoneDeviceDetail"] dataUsingEncoding:NSUTF8StringEncoding]];
        //        [postbody appendData:[[NSString stringWithFormat:@"&Deviceid=%@",[Utils getValueFronUserDefault:aDeviceId]] dataUsingEncoding:NSUTF8StringEncoding]];
        //        [postbody appendData:[[NSString stringWithFormat:@"&Devicetoken=%@",newToken] dataUsingEncoding:NSUTF8StringEncoding]];
        //        returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[Utils getAppendURLForConsumer:NO]];
    }
}
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:
(NSDictionary *)userInfo
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
//    UIAlertView *errorAlert = [[UIAlertView alloc]
//                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
//    CLLocation *currentLocation = newLocation;
    
//    if (currentLocation != nil) {
//        [Utils setValueToUserDefault:[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude ] andKey:aCurrentLat];
//        [Utils setValueToUserDefault:[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude ] andKey:aCurrentLong];
//    }
}
- (CLLocationManager *)deviceLocation {
    
    if ([_locationManager respondsToSelector:
         @selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
    return _locationManager;
}


@end
