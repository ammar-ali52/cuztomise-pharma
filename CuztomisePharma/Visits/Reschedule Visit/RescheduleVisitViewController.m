//
//  RescheduleVisitViewController.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 26/03/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "RescheduleVisitViewController.h"

@interface RescheduleVisitViewController ()

@end

@implementation RescheduleVisitViewController
@synthesize cityString;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        workOrderArray = [[NSMutableArray alloc] init];
        workOrderManager = [[WorkOrderManager alloc] init];
        originalArray=[[NSMutableArray alloc] init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    workOrderArray=[workOrderManager getAllWorkOrderbyStatus:[NSNumber numberWithInt:2 ]andFilteredCity:cityString];
    if (workOrderArray.count>0) {
        recordLabel.hidden=YES;
    }
    originalArray=workOrderArray;
   // rescheduleTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated{
    workOrderArray=[workOrderManager getAllWorkOrderbyStatus:[NSNumber numberWithInt:2 ]andFilteredCity:cityString];
    if (workOrderArray.count>0) {
        recordLabel.hidden=YES;
    }
    originalArray=workOrderArray;
    rescheduleTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [rescheduleTableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [workOrderArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0) {
        return 0;
    }
    else
        return 10; // you can have your own choice, of course
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"VisitCellTableViewCell";
    
    VisitCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        NSArray * nibArray=[[NSBundle mainBundle] loadNibNamed:@"VisitCellTableViewCell" owner:self options:nil];
        cell = [nibArray objectAtIndex:0];
    }
    WorkOrder * workOrderObject=[workOrderArray objectAtIndex:indexPath.section];
    cell.cellNameTextView.text=workOrderObject.name;
    cell.cellQualificationTextView.text=workOrderObject.qualification;
    cell.cellLocationNameTextView.text=workOrderObject.city;
    cell.cellDateTextView.text=[Utils dateFromString:workOrderObject.startDate byFormatter:@"dd/MM/yyyy"];
    cell.cellNameTextView.font=[UIFont appLargeFont];
    cell.cellQualificationTextView.font=[UIFont appNormalFont];
    cell.cellLocationNameTextView.font=[UIFont appNormalFont];
    cell.cellDateTextView.font=[UIFont appNormalFont];
    

    switch ([workOrderObject.status intValue]) {
        case aClosedWorkOrder:
            cell.cellRibbonImageView.image=[UIImage imageNamed:@"grey_flag_1x.png"];
            break;
        case aSkipWorkOrder:
            cell.cellRibbonImageView.image=[UIImage imageNamed:@"red_flag_1x.png"];
            break;
        case aRescheduleWorkOrder:
            
            cell.cellRibbonImageView.image=[UIImage imageNamed:@"yellow_flag_1x.png"];
            break;;
        case aNewWorkOrder:
            
            cell.cellRibbonImageView.image=[UIImage imageNamed:@"green_flag_1x.png"];
            break;;
            
        default:
            break;
    }
    
    
    
    //
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    WorkOrder * workOrderObject=[workOrderArray objectAtIndex:indexPath.section];
    switch ([workOrderObject.status intValue]) {
        case aClosedWorkOrder:
        {
            UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Visit Close" message:[NSString stringWithFormat:@"Other Notes:- %@ \nDrug Prescribed:- %@\nDrug Promoted:- %@,%@",workOrderObject.extraInfo,workOrderObject.drugPrescribed,workOrderObject.drugPromoted,workOrderObject.drugSuggest] delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            break;
        }
        case aSkipWorkOrder:
        {
            UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Visit Skipped" message:[NSString stringWithFormat:@"Reason :- %@",workOrderObject.reason] delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            break;
        }
        case aRescheduleWorkOrder:
        {
            UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Visit Rescheduled" message:[NSString stringWithFormat:@"Rescheduled:- %@",[Utils dateFromString:workOrderObject.lastUpdate byFormatter:@"dd-MM-yyyy"]] delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            break;
        }
        case aNewWorkOrder:
        {
//            RowClickPopoverViewController *detailViewController = [[RowClickPopoverViewController alloc] initWithNibName:@"RowClickPopoverViewController" bundle:nil];
//            detailViewController.workOrderObject=[workOrderArray objectAtIndex:indexPath.section];
//            [self presentPopupViewController:detailViewController animationType:(int)indexPath.row];
//
//            break;
        }
            
        default:
            break;
    }
        //    RowClickPopoverViewController *detailViewController = [[RowClickPopoverViewController alloc] initWithNibName:@"RowClickPopoverViewController" bundle:nil];
    //    [self presentPopupViewController:detailViewController animationType:(int)indexPath.row];
}
//- (void)cancelButtonClicked:(MJSecondDetailViewController *)aSecondDetailViewController
//{
//    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
//}

#pragma mark - search bar
- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    [theSearchBar resignFirstResponder];
    theSearchBar.showsCancelButton=NO;

    NSString *searchString = searchBar.text;
    if (searchString.length>0) {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchString];
        workOrderArray =(NSMutableArray *) [originalArray filteredArrayUsingPredicate:resultPredicate];
        [rescheduleTableView reloadData];
    }
    else
    {
        workOrderArray=originalArray;
        [rescheduleTableView reloadData];

    }
    if (workOrderArray.count==0) {
        recordLabel.hidden=NO;
    }
    else
    {
        recordLabel.hidden=YES;
    }

    NSLog(@"%@",searchString);
   
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar
{
    theSearchBar.showsCancelButton=YES;
    
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar1
{
    [searchBar1 resignFirstResponder];
    NSString *searchString = searchBar.text;
    searchBar1.showsCancelButton=NO;

    if (searchString.length>0) {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchString];
        workOrderArray =(NSMutableArray *) [originalArray filteredArrayUsingPredicate:resultPredicate];
        [rescheduleTableView reloadData];
    }
    else
    {
        workOrderArray=originalArray;
        [rescheduleTableView reloadData];
        
    }
    if (workOrderArray.count==0) {
        recordLabel.hidden=NO;
    }
    else
    {
        recordLabel.hidden=YES;
    }

}

- (void)searchBar:(UISearchBar *)searchBar1 textDidChange:(NSString *)searchText
{
    if (searchText.length>0) {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
        workOrderArray =(NSMutableArray *) [originalArray filteredArrayUsingPredicate:resultPredicate];
        [rescheduleTableView reloadData];
    }
    else
    {
        workOrderArray=originalArray;
        [rescheduleTableView reloadData];
        
    }
    if (workOrderArray.count==0) {
        recordLabel.hidden=NO;
    }
    else
    {
        recordLabel.hidden=YES;
    }
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)theSearchBar
{
    
    [theSearchBar resignFirstResponder];
    NSString *searchString = theSearchBar.text;
    if (searchString.length>0) {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchString];
        workOrderArray =(NSMutableArray *) [originalArray filteredArrayUsingPredicate:resultPredicate];
        [rescheduleTableView reloadData];
    }
    else
    {
        workOrderArray=originalArray;
        [rescheduleTableView reloadData];
        
    }
    if (workOrderArray.count==0) {
        recordLabel.hidden=NO;
    }
    else
    {
        recordLabel.hidden=YES;
    }
    
}
@end
