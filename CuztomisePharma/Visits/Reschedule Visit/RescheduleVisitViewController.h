//
//  RescheduleVisitViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 26/03/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"

@interface RescheduleVisitViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    IBOutlet UITableView *rescheduleTableView;
    NSMutableArray * workOrderArray;
    WorkOrderManager * workOrderManager;
    NSMutableArray * originalArray;

    IBOutlet UISearchBar *searchBar;
    IBOutlet UILabel *recordLabel;
    
}
@property (nonatomic,retain)NSString * cityString;

@end
