//
//  VisitsViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 23/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"
@class TodayVisitViewController;
@class ClosedVisitViewController;
@class RescheduleVisitViewController;
@interface VisitsViewController : UIViewController<UIScrollViewDelegate,UIActionSheetDelegate>
{
    TodayVisitViewController * todayViewController;
    ClosedVisitViewController * closedViewController;
    RescheduleVisitViewController * rescheduleViewController;
    UIImageView * secondImageView;
    UIImageView * selectedImageView;
    UIImageView * thirdImageView;
    int buttonTag;
    NSMutableString * cityString;
    
}
@property (nonatomic, strong) UIScrollView *scroll;
@end
