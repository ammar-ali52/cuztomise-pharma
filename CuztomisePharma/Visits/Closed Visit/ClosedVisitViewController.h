//
//  ClosedVisitViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 26/03/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"
@class WorkOrderManager;

@interface ClosedVisitViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    IBOutlet UITableView *closedTableView;
    NSMutableArray * workOrderArray;
    WorkOrderManager * workOrderManager;
    NSMutableArray * originalArray;
    IBOutlet UILabel *recordLabel;
    IBOutlet UISearchBar *searchView;
}
@property (nonatomic,retain)NSString * cityString;

@end
