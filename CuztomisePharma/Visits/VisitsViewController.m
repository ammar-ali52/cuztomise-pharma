//
//  VisitsViewController.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 23/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import "VisitsViewController.h"

@interface VisitsViewController ()

@end

@implementation VisitsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeModelController:) name:@"RemoveModelView" object:nil];
    cityString=[[NSMutableString alloc] init];
    buttonTag=0;

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,0,140,42)];
    titleLabel.text=@"Visits";
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor appWhiteColor];
    titleLabel.backgroundColor=[UIColor appClearColor];
    self.navigationItem.titleView = titleLabel;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    UIButton * firstButton=[[UIButton alloc] initWithFrame:CGRectMake(0,65 , screenWidth/3, 35)];
    [firstButton setBackgroundColor:[UIColor  appGreenColor]];
    [firstButton setTitle:@"Visits Today" forState:UIControlStateNormal];
    [firstButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [firstButton setTag:0];
    firstButton.titleLabel.font = [UIFont appNormalFont];
 
    [firstButton addTarget:self action:@selector(firstButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:firstButton];
     selectedImageView=[[UIImageView alloc] initWithFrame:CGRectMake((screenWidth/6),95 , 10, 6)];
    [selectedImageView setImage:[UIImage imageNamed:@"tab-selector.png"]];
    [self.view addSubview:selectedImageView];
    
    UIButton * secondButton=[[UIButton alloc] initWithFrame:CGRectMake(screenWidth/3,65 , screenWidth/3, 35)];
    [secondButton setBackgroundColor:[UIColor appGreenColor]];
    [secondButton setTitle:@"Visits Closed" forState:UIControlStateNormal];
    [secondButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [secondButton setTag:1];
    secondButton.titleLabel.font = [UIFont appNormalFont];
    
    [secondButton addTarget:self action:@selector(firstButtonAction:) forControlEvents:UIControlEventTouchUpInside];
   
    [self.view addSubview:secondButton];
     secondImageView=[[UIImageView alloc] initWithFrame:CGRectMake((screenWidth-screenWidth/2),95 , 10, 6)];
    [secondImageView setImage:[UIImage imageNamed:@"tab-selector.png"]];
    [self.view addSubview:secondImageView];
    [secondImageView setHidden:YES];
    
    UIButton * thirdButton=[[UIButton alloc] initWithFrame:CGRectMake(screenWidth-screenWidth/3,65 , screenWidth/3, 35)];
    [thirdButton setBackgroundColor:[UIColor appGreenColor]];
    
    [thirdButton setTitle:@"All Visits" forState:UIControlStateNormal];
    [thirdButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [thirdButton setTag:2];
    thirdButton.titleLabel.font = [UIFont appNormalFont];
    
    [thirdButton addTarget:self action:@selector(firstButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:thirdButton];
    thirdImageView=[[UIImageView alloc] initWithFrame:CGRectMake((screenWidth-screenWidth/6),95 , 10, 6)];
    [thirdImageView setImage:[UIImage imageNamed:@"tab-selector.png"]];
    [self.view addSubview:thirdImageView];
    [thirdImageView setHidden:YES];

    //
    //    firstLineImageView=[[UIImageView alloc] initWithFrame:CGRectMake(0,99 , screenWidth/2, 1)];
    //    [firstLineImageView setBackgroundColor:[UIColor blueColor]];
    //    [self.view addSubview:firstLineImageView];
    //
    //    secondLineImageView=[[UIImageView alloc] initWithFrame:CGRectMake(screenWidth/2,99 , screenWidth/2, 1)];
    //    [secondLineImageView setBackgroundColor:[UIColor blueColor]];
    //    [self.view addSubview:secondLineImageView];
    //
    //    thirdLineImageView=[[UIImageView alloc] initWithFrame:CGRectMake(screenWidth-screenWidth/3,99 , screenWidth/3, 1)];
    //    [thirdLineImageView setBackgroundColor:[UIColor blueColor]];
    //    [self.view addSubview:thirdLineImageView];
    
    //    [secondLineImageView setHidden:YES];
    //    [thirdLineImageView setHidden:YES];
    
        UIButton * infoBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
        [infoBtn setImage:[UIImage imageNamed:@"dot.png"] forState:UIControlStateNormal];
        [infoBtn addTarget:self action:@selector(filterButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [infoBtn setFrame:CGRectMake(2,10,8, 30)];
        UIBarButtonItem *infoBarButton=[[UIBarButtonItem alloc]initWithCustomView:infoBtn];
//        
//        UIButton * syncBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
//        [syncBtn setImage:[UIImage imageNamed:@"sync.png"] forState:UIControlStateNormal];
//        [syncBtn addTarget:self action:@selector(syncVisitButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//        [syncBtn setFrame:CGRectMake(35,13,20, 25)];
//        UIBarButtonItem *syncBtnBarButton=[[UIBarButtonItem alloc]initWithCustomView:syncBtn];
//    
//        UIButton * addBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
//        [addBtn setImage:[UIImage imageNamed:@"add-tile.png"] forState:UIControlStateNormal];
//        [addBtn addTarget:self action:@selector(addVisitButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//        [addBtn setFrame:CGRectMake(35,13,30, 30)];
//        UIBarButtonItem *addBtnBarButton=[[UIBarButtonItem alloc]initWithCustomView:addBtn];
    
        self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:infoBarButton,nil];
    

    
}
-(void)viewDidAppear:(BOOL)animated
{
    [self loadScroll];

}


- (void)loadScroll
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
 
    _scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 100, screenWidth, screenHeight-100)];
    [_scroll setBackgroundColor:[UIColor clearColor]];
    [_scroll setPagingEnabled:YES];
    [_scroll setBounces:NO];
    [_scroll setScrollEnabled:YES];
    [_scroll setShowsHorizontalScrollIndicator:NO];
    [_scroll setShowsVerticalScrollIndicator:NO];
    [_scroll setDelegate:self];
    if (screenWidth<=320) {
        [_scroll setContentSize:CGSizeMake(320*3, 380)];

    }
    else
    {
        [_scroll setContentSize:CGSizeMake(375*3, 380)];
    }
    [self.view addSubview:_scroll];
    
    todayViewController=[[TodayVisitViewController alloc] initWithNibName:@"TodayVisitViewController" bundle:[NSBundle mainBundle]];
//    userChatViewController.delegate=self;
    todayViewController.cityString=cityString;
    todayViewController.view.frame=CGRectMake(0,0, screenWidth, screenHeight-100);
    [_scroll addSubview:todayViewController.view];
    closedViewController=[[ClosedVisitViewController alloc] initWithNibName:@"ClosedVisitViewController" bundle:[NSBundle mainBundle]];
//    groupChatViewController.delegate=self;
    closedViewController.cityString=cityString;
    closedViewController.view.frame=CGRectMake(screenWidth,0, screenWidth, screenHeight-100);
    [_scroll addSubview:closedViewController.view];
    
    rescheduleViewController=[[RescheduleVisitViewController alloc] initWithNibName:@"RescheduleVisitViewController" bundle:[NSBundle mainBundle]];
    rescheduleViewController.cityString=cityString;
    rescheduleViewController.view.frame=CGRectMake(screenWidth*2,0, screenWidth, screenHeight-200);
    [_scroll addSubview:rescheduleViewController.view];
      [_scroll setContentOffset:CGPointMake(buttonTag*screenWidth, _scroll.contentOffset.y) animated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)removeModelController:(NSNotification* )note
{
    NSString * pageOption=note.object[@"Screen"] ;
    WorkOrder * workOrderObject=note.object[@"workOrder"] ;
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    if ([pageOption intValue]==0) {
    
        DashboardViewController * dashboardView=[[DashboardViewController alloc] init];
        VisitsViewController * visitView=[[VisitsViewController alloc] init];
        DoctorsViewController * doctorView=[[DoctorsViewController alloc] init];
        CalenderViewController * calenderView=[[CalenderViewController alloc] init];
        WallViewController * wallView=[[WallViewController alloc] init];
        SettingViewController * settingView=[[SettingViewController alloc] init];
        SalesViewController * salesView=[[SalesViewController alloc] init];
        AdminViewController * adminView=[[AdminViewController alloc] init];
        LogoutViewController * logoutView=[[LogoutViewController alloc] init];
        //    RootController *menuController = [[RootController alloc]
        //                                      initWithViewControllers:@[dashboardView]
        //                                      andMenuTitles:@[@"Duties",]];
        GoToVIsitViewController * gotoVisit=[[GoToVIsitViewController alloc] init];
        gotoVisit.workOrderObject=workOrderObject;

        RootController *menuController = [[RootController alloc]
                                          initWithViewControllers:@[dashboardView,visitView,doctorView,calenderView,wallView,settingView,salesView,adminView,logoutView,gotoVisit]
                                          andMenuTitles:@[@"Duties",@"Home",@"Visits",@"Doctors",@"Calender",@"Wall",@"Syncronize",@"Sales",@"Logout",] andCurrentPage:9];
        self.navigationController.navigationBar.hidden = YES;
        [self.navigationController pushViewController:menuController animated:YES];
    }
    else
    {
        DashboardViewController * dashboardView=[[DashboardViewController alloc] init];
        VisitsViewController * visitView=[[VisitsViewController alloc] init];
        DoctorsViewController * doctorView=[[DoctorsViewController alloc] init];
        CalenderViewController * calenderView=[[CalenderViewController alloc] init];
        WallViewController * wallView=[[WallViewController alloc] init];
        SettingViewController * settingView=[[SettingViewController alloc] init];
        SalesViewController * salesView=[[SalesViewController alloc] init];
        AdminViewController * adminView=[[AdminViewController alloc] init];
        LogoutViewController * logoutView=[[LogoutViewController alloc] init];
        //    RootController *menuController = [[RootController alloc]
        //                                      initWithViewControllers:@[dashboardView]
        //                                      andMenuTitles:@[@"Duties",]];
        SkipVisitViewController * skipVisit=[[SkipVisitViewController alloc] init];
        skipVisit.workOrderObject=workOrderObject;
        skipVisit.isFromMainView=YES;
        RootController *menuController = [[RootController alloc]
                                          initWithViewControllers:@[dashboardView,visitView,doctorView,calenderView,wallView,settingView,salesView,adminView,logoutView,skipVisit]
                                          andMenuTitles:@[@"Duties",@"Home",@"Visits",@"Doctors",@"Calender",@"Wall",@"Syncronize",@"Sales",@"Logout",] andCurrentPage:9];
    
        self.navigationController.navigationBar.hidden = YES;
        [self.navigationController pushViewController:menuController animated:YES];
    }
 

}
- (IBAction)firstButtonAction:(id)sender {
    
    buttonTag=(int)[sender tag];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
     [_scroll setContentOffset:CGPointMake(buttonTag*screenWidth, _scroll.contentOffset.y) animated:YES];
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        [_scroll setContentOffset:CGPointMake(buttonTag*768, _scroll.contentOffset.y) animated:YES];
//        
//    }
//    else
//    {
//        
//        [_scroll setContentOffset:CGPointMake(buttonTag*320, _scroll.contentOffset.y) animated:YES];
//        
//    }
    switch (buttonTag) {
        case 0:
            [thirdImageView setHidden:YES];
            [selectedImageView setHidden:NO];
            [secondImageView setHidden:YES];

            break;
        case 1:
            [thirdImageView setHidden:YES];
            [selectedImageView setHidden:YES];
            [secondImageView setHidden:NO];
            break;
        case 2:
            [thirdImageView setHidden:NO];
            [selectedImageView setHidden:YES];
            [secondImageView setHidden:YES];
            break;
            
        default:
            break;
    }
}
#pragma mark - Scroll View Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offSet= scrollView.contentOffset.x;
    if (offSet<320) {
        buttonTag=0;
        [thirdImageView setHidden:YES];
        [selectedImageView setHidden:NO];
        [secondImageView setHidden:YES];
        

    }
    else if (offSet<640) {
        buttonTag=1;
        [thirdImageView setHidden:YES];
        [selectedImageView setHidden:YES];
        [secondImageView setHidden:NO];
    }
    else
    {
        buttonTag=2;
        [thirdImageView setHidden:NO];
        [selectedImageView setHidden:YES];
        [secondImageView setHidden:YES];
    }
    
}
- (IBAction)filterButtonClick:(id)sender
{
    WorkOrderManager * workOrderManager=[[WorkOrderManager alloc] init];
    
    
    UIActionSheet *tempSheet = [[UIActionSheet alloc] initWithTitle:@"Select City" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:nil];
    NSMutableArray * workOrderArray;

    switch (buttonTag) {
        case 0:
            workOrderArray=[workOrderManager getDistinctCity:[NSNumber numberWithInt:aNewWorkOrder]];
            break;
        case 1:
            workOrderArray=[workOrderManager getDistinctCity:[NSNumber numberWithInt:aClosedWorkOrder]];
            break;
        case 2:
            workOrderArray=[workOrderManager getDistinctCity:[NSNumber numberWithInt:1]];
            break;
            
        default:
            break;
    }
    
    for (NSMutableDictionary * tempDic in workOrderArray)
    {
            [tempSheet addButtonWithTitle:[tempDic objectForKey:@"city"]];
    }
    [tempSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [tempSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)theActionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"Button Index %ld",(long)buttonIndex);
    if (buttonIndex==0) {
        cityString=(NSMutableString *)@"";
        NSLog(@"%@",cityString);
        [self loadScroll];

    }
    else
    {
       cityString=(NSMutableString *)[theActionSheet buttonTitleAtIndex:buttonIndex];
        NSLog(@"%@",cityString);
        [self loadScroll];
    }
}
- (IBAction)syncVisitButtonClick:(id)sender
{
    
}
- (IBAction)addVisitButtonClick:(id)sender
{
    
    
}
@end
