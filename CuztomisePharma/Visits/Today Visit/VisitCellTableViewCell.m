//
//  VisitCellTableViewCell.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 18/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "VisitCellTableViewCell.h"

@implementation VisitCellTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
