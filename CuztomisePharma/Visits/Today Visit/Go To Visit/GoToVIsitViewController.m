//
//  GoToVIsitViewController.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 29/03/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "GoToVIsitViewController.h"

@interface GoToVIsitViewController ()

@end

@implementation GoToVIsitViewController
@synthesize workOrderObject;
- (void)viewDidLoad {
    [super viewDidLoad];
    isIamHereSelected=NO;
    doctorNameTextView.text=workOrderObject.name;
    doctorAddressTextView.text=workOrderObject.location;
    doctorEmailTextView.text=workOrderObject.email;
    doctorPhoneTextView.text=workOrderObject.contactNo;
    clinicAddresst.text=workOrderObject.location;
    contactEmailTextView.text=@"Test";
    contactPhoneNumberTextView.text=@"Test";
    noteTextView.text=workOrderObject.notes;
    orderNumberTextView.text=workOrderObject.orderNo;
    showTimeTextView.text=workOrderObject.estimatedWorkTime;
    workOrderManager=[[WorkOrderManager alloc] init];

    doctorNameTextView.font=[UIFont appNormalFont];
    
    contactPhoneNumberTextView.font=[UIFont appNormalFont];
    contactEmailTextView.font=[UIFont appNormalFont];
    clinicAddresst.font=[UIFont appNormalFont];
    doctorPhoneTextView.font=[UIFont appNormalFont];
    doctorEmailTextView.font=[UIFont appNormalFont];
    doctorAddressTextView.font=[UIFont appNormalFont];
    noteTextView.font=[UIFont appNormalFont];
    orderNumberTextView.font=[UIFont appNormalFont];
    showTimeTextView.font=[UIFont appNormalFont];
    
    UIButton * backBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"arrow-left.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10,13,20, 30)];
    UIBarButtonItem *backBtnBarButton=[[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:backBtnBarButton,nil];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated{
    if ([workOrderObject.isMonthVisit intValue]==1) {
        isIamHereSelected=YES;
    }
    else
    {
        [iAmHereButton setBackgroundColor:[UIColor appGreenColor]];
        isIamHereSelected=NO;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closedVisitAction:(id)sender {
    if (isIamHereSelected) {
        UIActionSheet *tempSheet = [[UIActionSheet alloc] initWithTitle:@"Were you able to meet doctor" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Yes",@"No",nil];
        [tempSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
        [tempSheet showInView:self.view];
    }
    else
    {
        UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"" message:@"Please click on I Am Here Button" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
  
}
- (void)actionSheet:(UIActionSheet *)theActionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    workOrderObject.notes=noteTextView.text;
    [workOrderManager updateWorkOrder:workOrderObject];
    NSLog(@"Button Index %ld",(long)buttonIndex);
    if (buttonIndex==0) {
        FeedBackViewController * feedBackViewController=[[FeedBackViewController alloc] initWithNibName:@"FeedBackViewController" bundle:[NSBundle mainBundle]];
        feedBackViewController.workOrderObject=workOrderObject;
        [self.navigationController pushViewController:feedBackViewController animated:YES];
    }
    else if (buttonIndex==1)
    {
        SkipVisitViewController * skipVisitViewController=[[SkipVisitViewController alloc] initWithNibName:@"SkipVisitViewController" bundle:[NSBundle mainBundle]];
        skipVisitViewController.workOrderObject=workOrderObject;
        skipVisitViewController.isFromMainView=NO;
        [self.navigationController pushViewController:skipVisitViewController animated:YES];

    }
}

- (IBAction)iAmHereBtnAction:(id)sender {
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude: [workOrderObject.latitude doubleValue] longitude:[workOrderObject.longitude doubleValue]];
    AppDelegate * appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    CLLocationManager * locaton=[appDelegate deviceLocation];

    CLLocation *location2 = [[CLLocation alloc] initWithLatitude: locaton.location.coordinate.latitude longitude:locaton.location.coordinate.longitude];
    float distInMeter = [location1 distanceFromLocation:location2];
    float getRadius=[[Utils getValueFronUserDefault:aRadius] floatValue];

    if (distInMeter<getRadius*100) {
        workOrderObject.dateCreated=[NSDate date];
        isIamHereSelected=YES;
        [iAmHereButton setBackgroundColor:[UIColor blackColor]];
        workOrderObject.mrLatLongFrom=[NSString stringWithFormat:@"%f,%f",locaton.location.coordinate.latitude,locaton.location.coordinate.longitude];
        [workOrderObject setIsMonthVisit:[NSNumber numberWithInt:1]];
        [workOrderManager updateWorkOrder:workOrderObject];
    }
    else
    {
        UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Location" message:@"You are not in range of visit" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}
- (IBAction)backBtnAction:(id)sender {
    DashboardViewController * dashboardView=[[DashboardViewController alloc] init];
    VisitsViewController * visitView=[[VisitsViewController alloc] init];
    DoctorsViewController * doctorView=[[DoctorsViewController alloc] init];
    CalenderViewController * calenderView=[[CalenderViewController alloc] init];
    WallViewController * wallView=[[WallViewController alloc] init];
    SettingViewController * settingView=[[SettingViewController alloc] init];
    SalesViewController * salesView=[[SalesViewController alloc] init];
    AdminViewController * adminView=[[AdminViewController alloc] init];
    LogoutViewController * logoutView=[[LogoutViewController alloc] init];
    //    RootController *menuController = [[RootController alloc]
    //                                      initWithViewControllers:@[dashboardView]
    //                                      andMenuTitles:@[@"Duties",]];
    GoToVIsitViewController * gotoVisit=[[GoToVIsitViewController alloc] init];
    
    RootController *menuController = [[RootController alloc]
                                      initWithViewControllers:@[dashboardView,visitView,doctorView,calenderView,wallView,settingView,salesView,adminView,logoutView,gotoVisit]
                                      andMenuTitles:@[@"Duties",@"Home",@"Visits",@"Doctors",@"Calender",@"Wall",@"Syncronize",@"Sales",@"Logout",] andCurrentPage:1];
    self.navigationController.navigationBar.hidden = YES;
    [self.navigationController pushViewController:menuController animated:YES];
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView;
{
    [textView resignFirstResponder];
    return YES;
}
@end
