//
//  GoToVIsitViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 29/03/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"

@interface GoToVIsitViewController : UIViewController<UIActionSheetDelegate,UITextViewDelegate>
{
    IBOutlet UILabel *doctorNameTextView;
    
    IBOutlet UILabel *contactPhoneNumberTextView;
    IBOutlet UILabel *contactEmailTextView;
    IBOutlet UILabel *clinicAddresst;
    IBOutlet UILabel *doctorPhoneTextView;
    IBOutlet UILabel *doctorEmailTextView;
    IBOutlet UILabel *doctorAddressTextView;
    IBOutlet UITextView *noteTextView;
    IBOutlet UILabel *orderNumberTextView;
    IBOutlet UIButton *iAmHereButton;
    IBOutlet UIButton *closeVisitButton;
    IBOutlet UILabel *showTimeTextView;
    BOOL isIamHereSelected;
    WorkOrderManager * workOrderManager;
}
- (IBAction)iAmHereBtnAction:(id)sender;

@property (nonatomic,retain)WorkOrder * workOrderObject;
- (IBAction)closedVisitAction:(id)sender;

@end
