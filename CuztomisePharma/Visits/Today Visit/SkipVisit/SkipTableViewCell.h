//
//  SkipTableViewCell.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 13/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SkipTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *reasonName;
@property (strong, nonatomic) IBOutlet UIButton *reasonRadioButton;

@end
