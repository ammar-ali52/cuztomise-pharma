//
//  SkipVisitViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 13/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"
@class ReasonManager;

@interface SkipVisitViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UIPickerViewDataSource,UIToolbarDelegate,UITextViewDelegate>
{
    NSArray * reasonArray;
    ReasonManager * reasonManager;
    NSIndexPath* checkedIndexPath;
    IBOutlet UIButton *submitButton;
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIToolbar *toolBar;
    IBOutlet UITextView *otherReasonTextView;
    NSString * reasonString;
}
- (IBAction)submitButtonAction:(id)sender;
@property (nonatomic,retain)WorkOrder * workOrderObject;
@property (nonatomic, retain) NSIndexPath* checkedIndexPath;
@property(nonatomic)Boolean isFromMainView;
- (IBAction)doneButtonAction:(id)sender;
- (IBAction)cancelButtonAction:(id)sender;

@end
