//
//  SkipVisitViewController.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 13/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "SkipVisitViewController.h"

@interface SkipVisitViewController ()

@end

@implementation SkipVisitViewController

@synthesize workOrderObject,checkedIndexPath,isFromMainView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        reasonArray=[[NSMutableArray alloc] init];
        reasonManager=[[ReasonManager alloc] init];
        reasonString=[[NSString alloc] init];
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    reasonArray=[reasonManager getAllReason];
    self.navigationController.navigationBar.hidden = NO;
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,0,140,42)];
    titleLabel.text=@"Reasons";
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor appWhiteColor];
    titleLabel.backgroundColor=[UIColor appClearColor];
    self.navigationItem.titleView = titleLabel;
    if (isFromMainView) {
        UIButton * backBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
        [backBtn setImage:[UIImage imageNamed:@"arrow-left.png"] forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [backBtn setFrame:CGRectMake(10,13,20, 30)];
        UIBarButtonItem *backBtnBarButton=[[UIBarButtonItem alloc]initWithCustomView:backBtn];
        
        self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:backBtnBarButton,nil];
    }
    else
    {
        UIButton * backBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
        [backBtn setImage:[UIImage imageNamed:@"arrow-left.png"] forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        [backBtn setFrame:CGRectMake(10,13,20, 30)];
        UIBarButtonItem *backBtnBarButton=[[UIBarButtonItem alloc]initWithCustomView:backBtn];
        
        self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:backBtnBarButton,nil];
    }

    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [datePicker setHidden:YES];
    [toolBar setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [reasonArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SkipTableViewCell";
    
    SkipTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        NSArray * nibArray=[[NSBundle mainBundle] loadNibNamed:@"SkipTableViewCell" owner:self options:nil];
        cell = [nibArray objectAtIndex:0];
    }
    Reason * reasonObject=[reasonArray objectAtIndex:indexPath.row];
    cell.reasonName.text=reasonObject.reason;
    cell.reasonName.font=[UIFont appNormalFont];

    if([self.checkedIndexPath isEqual:indexPath])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    //
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.checkedIndexPath)
    {
        UITableViewCell* uncheckCell = [tableView
                                        cellForRowAtIndexPath:self.checkedIndexPath];
        uncheckCell.accessoryType = UITableViewCellAccessoryNone;
        reasonString=@"";
    }
    if([self.checkedIndexPath isEqual:indexPath])
    {
        self.checkedIndexPath = nil;
    }
    else
    {
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.checkedIndexPath = indexPath;
        Reason * reasonObject=[reasonArray objectAtIndex:indexPath.row];
        reasonString=reasonObject.reason;
    }
}

- (IBAction)submitButtonAction:(id)sender {
      [self.view setFrame:CGRectMake(0,0,380,700)];
        [otherReasonTextView resignFirstResponder];
        if (reasonString.length!=0) {
            workOrderObject.notes=otherReasonTextView.text;
            WorkOrderManager * workOrderManager=[[WorkOrderManager alloc] init];
            [workOrderManager updateWorkOrder:workOrderObject];
            UIActionSheet *tempSheet = [[UIActionSheet alloc] initWithTitle:@"Are you want to reschedule this visit" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Yes",@"No",nil];
            [tempSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
            [tempSheet showInView:self.view];

        }
        else
        {
            UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Reason" message:@"Pleasr select reason to skip the visit" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];       
        }
   }
- (void)actionSheet:(UIActionSheet *)theActionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"Button Index %ld",(long)buttonIndex);
    if (buttonIndex==0)
    {
        [datePicker setHidden:NO];
        [toolBar setHidden:NO];
        [submitButton setHidden:YES];
        [otherReasonTextView setEditable:NO];
       
    }
    else if (buttonIndex==1)
    {
        WorkOrderManager * workOrderManager=[[WorkOrderManager alloc] init];
        [workOrderObject setStatus:[NSNumber numberWithInt:40]];
        workOrderObject.reason=reasonString;
        [workOrderManager updateWorkOrder:workOrderObject];
        DashboardViewController * dashboardView=[[DashboardViewController alloc] init];
        VisitsViewController * visitView=[[VisitsViewController alloc] init];
        DoctorsViewController * doctorView=[[DoctorsViewController alloc] init];
        CalenderViewController * calenderView=[[CalenderViewController alloc] init];
        WallViewController * wallView=[[WallViewController alloc] init];
        SettingViewController * settingView=[[SettingViewController alloc] init];
        SalesViewController * salesView=[[SalesViewController alloc] init];
        AdminViewController * adminView=[[AdminViewController alloc] init];
        LogoutViewController * logoutView=[[LogoutViewController alloc] init];
        //    RootController *menuController = [[RootController alloc]
        //                                      initWithViewControllers:@[dashboardView]
        //                                      andMenuTitles:@[@"Duties",]];
        GoToVIsitViewController * gotoVisit=[[GoToVIsitViewController alloc] init];
        
        RootController *menuController = [[RootController alloc]
                                          initWithViewControllers:@[dashboardView,visitView,doctorView,calenderView,wallView,settingView,salesView,adminView,logoutView,gotoVisit]
                                          andMenuTitles:@[@"Duties",@"Home",@"Visits",@"Doctors",@"Calender",@"Wall",@"Syncronize",@"Sales",@"Logout",] andCurrentPage:1];
        self.navigationController.navigationBar.hidden = YES;
        [self.navigationController pushViewController:menuController animated:YES];
    }
}
- (IBAction)doneButtonAction:(id)sender {
    [datePicker setHidden:YES];
    [toolBar setHidden:YES];
    [submitButton setHidden:NO];
    [otherReasonTextView setEditable:YES];
    WorkOrderManager * workOrderManager=[[WorkOrderManager alloc] init];
    [workOrderObject setStatus:[NSNumber numberWithInt:30]];
    [workOrderObject setLastUpdate:[datePicker date]];
    [workOrderManager updateWorkOrder:workOrderObject];
    DashboardViewController * dashboardView=[[DashboardViewController alloc] init];
    VisitsViewController * visitView=[[VisitsViewController alloc] init];
    DoctorsViewController * doctorView=[[DoctorsViewController alloc] init];
    CalenderViewController * calenderView=[[CalenderViewController alloc] init];
    WallViewController * wallView=[[WallViewController alloc] init];
    SettingViewController * settingView=[[SettingViewController alloc] init];
    SalesViewController * salesView=[[SalesViewController alloc] init];
    AdminViewController * adminView=[[AdminViewController alloc] init];
    LogoutViewController * logoutView=[[LogoutViewController alloc] init];
    //    RootController *menuController = [[RootController alloc]
    //                                      initWithViewControllers:@[dashboardView]
    //                                      andMenuTitles:@[@"Duties",]];
    GoToVIsitViewController * gotoVisit=[[GoToVIsitViewController alloc] init];
    
    RootController *menuController = [[RootController alloc]
                                      initWithViewControllers:@[dashboardView,visitView,doctorView,calenderView,wallView,settingView,salesView,adminView,logoutView,gotoVisit]
                                      andMenuTitles:@[@"Duties",@"Home",@"Visits",@"Doctors",@"Calender",@"Wall",@"Syncronize",@"Sales",@"Logout",] andCurrentPage:1];
    self.navigationController.navigationBar.hidden = YES;
    [self.navigationController pushViewController:menuController animated:YES];
}

- (IBAction)cancelButtonAction:(id)sender
{
    [datePicker setHidden:YES];
    [toolBar setHidden:YES];
    [submitButton setHidden:NO];
    [otherReasonTextView setEditable:YES];
}
- (IBAction)backBtnAction:(id)sender {
    DashboardViewController * dashboardView=[[DashboardViewController alloc] init];
    VisitsViewController * visitView=[[VisitsViewController alloc] init];
    DoctorsViewController * doctorView=[[DoctorsViewController alloc] init];
    CalenderViewController * calenderView=[[CalenderViewController alloc] init];
    WallViewController * wallView=[[WallViewController alloc] init];
    SettingViewController * settingView=[[SettingViewController alloc] init];
    SalesViewController * salesView=[[SalesViewController alloc] init];
    AdminViewController * adminView=[[AdminViewController alloc] init];
    LogoutViewController * logoutView=[[LogoutViewController alloc] init];
    //    RootController *menuController = [[RootController alloc]
    //                                      initWithViewControllers:@[dashboardView]
    //                                      andMenuTitles:@[@"Duties",]];
    GoToVIsitViewController * gotoVisit=[[GoToVIsitViewController alloc] init];
    
    RootController *menuController = [[RootController alloc]
                                      initWithViewControllers:@[dashboardView,visitView,doctorView,calenderView,wallView,settingView,salesView,adminView,logoutView,gotoVisit]
                                      andMenuTitles:@[@"Duties",@"Home",@"Visits",@"Doctors",@"Calender",@"Wall",@"Syncronize",@"Sales",@"Logout",] andCurrentPage:1];
    self.navigationController.navigationBar.hidden = YES;
    [self.navigationController pushViewController:menuController animated:YES];
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
     [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"])
    {
         [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
        [textView resignFirstResponder];
    } return YES;
}


@end
