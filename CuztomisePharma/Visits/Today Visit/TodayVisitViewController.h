//
//  TodayVisitViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 26/03/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"
@class  WorkOrderManager;

@interface TodayVisitViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray * workOrderArray;
    WorkOrderManager * workOrderManager;
    IBOutlet UITableView *todayVisitTableView;
    IBOutlet UISearchBar *searchBar;
    NSMutableArray * originalArray;
    IBOutlet UILabel *recordLabel;
}
@property (nonatomic,retain)NSString * cityString;


@end
