//
//  VisitCellTableViewCell.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 18/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VisitCellTableViewCell : UITableViewCell
{
 }
@property (strong, nonatomic) IBOutlet UILabel *cellNameTextView;
@property (strong, nonatomic) IBOutlet UILabel *cellQualificationTextView;
@property (strong, nonatomic) IBOutlet UILabel *cellDateTextView;
@property (strong, nonatomic) IBOutlet UILabel *cellLocationNameTextView;
@property (strong, nonatomic) IBOutlet UIImageView *cellRibbonImageView;

@end
