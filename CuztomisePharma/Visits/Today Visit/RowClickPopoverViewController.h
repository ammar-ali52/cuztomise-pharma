//
//  RowClickPopoverViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 28/03/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"
@interface RowClickPopoverViewController : UIViewController
{
    IBOutlet UIButton *goToVisitButton;
    IBOutlet UIButton *skipVisitButton;
    IBOutlet UIImageView *middleLineImageView;
    
}
@property(nonatomic,retain)WorkOrder * workOrderObject;
- (IBAction)goToVisitAction:(id)sender;
- (IBAction)skipVisitAction:(id)sender;

@end
