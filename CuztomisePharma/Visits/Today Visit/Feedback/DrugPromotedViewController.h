//
//  DrugPromotedViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 15/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"
@interface DrugPromotedViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>
{
    NSMutableArray * drugArray;

    IBOutlet UITableView *drugPromotedTableView;
    NSMutableString * selectedDrugName;
    
}
@property (nonatomic, strong)WorkOrder * workOrderObject;

@end
