//
//  FeedBackViewController.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 15/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "FeedBackViewController.h"

@interface FeedBackViewController ()

@end

@implementation FeedBackViewController
@synthesize workOrderObject;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,0,140,42)];
    titleLabel.text=@"Feedback";
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor appWhiteColor];
    titleLabel.backgroundColor=[UIColor appClearColor];
    self.navigationItem.titleView = titleLabel;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    UIButton * firstButton=[[UIButton alloc] initWithFrame:CGRectMake(0,65 , screenWidth/3, 35)];
    [firstButton setBackgroundColor:[UIColor  appGreenColor]];
    [firstButton setTitle:@"Prescribe" forState:UIControlStateNormal];
    [firstButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [firstButton setTag:0];
    firstButton.titleLabel.font = [UIFont appNormalFont];
    
    
    [firstButton addTarget:self action:@selector(firstButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:firstButton];
    selectedImageView=[[UIImageView alloc] initWithFrame:CGRectMake((screenWidth/6),95 , 10, 6)];
    [selectedImageView setImage:[UIImage imageNamed:@"tab-selector.png"]];
    [self.view addSubview:selectedImageView];
    
    UIButton * secondButton=[[UIButton alloc] initWithFrame:CGRectMake(screenWidth/3,65 , screenWidth/3, 35)];
    [secondButton setBackgroundColor:[UIColor appGreenColor]];
    [secondButton setTitle:@"Promoted" forState:UIControlStateNormal];
    [secondButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [secondButton setTag:1];
    secondButton.titleLabel.font = [UIFont appNormalFont];
    
    [secondButton addTarget:self action:@selector(firstButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:secondButton];
    secondImageView=[[UIImageView alloc] initWithFrame:CGRectMake((screenWidth-screenWidth/2),95 , 10, 6)];
    [secondImageView setImage:[UIImage imageNamed:@"tab-selector.png"]];
    [self.view addSubview:secondImageView];
    [secondImageView setHidden:YES];
    
    UIButton * thirdButton=[[UIButton alloc] initWithFrame:CGRectMake(screenWidth-screenWidth/3,65 , screenWidth/3, 35)];
    [thirdButton setBackgroundColor:[UIColor appGreenColor]];
    
    [thirdButton setTitle:@"All Drug" forState:UIControlStateNormal];
    [thirdButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [thirdButton setTag:2];
    thirdButton.titleLabel.font = [UIFont appNormalFont];
    
    [thirdButton addTarget:self action:@selector(firstButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:thirdButton];
    thirdImageView=[[UIImageView alloc] initWithFrame:CGRectMake((screenWidth-screenWidth/6),95 , 10, 6)];
    [thirdImageView setImage:[UIImage imageNamed:@"tab-selector.png"]];
    [self.view addSubview:thirdImageView];
    [thirdImageView setHidden:YES];
    
    UIButton * backBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"arrow-left.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10,13,20, 30)];
    UIBarButtonItem *backBtnBarButton=[[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:backBtnBarButton,nil];
    
    
}
- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    [self loadScroll];
    
}


- (void)loadScroll
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    _scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 100, screenWidth, screenHeight-160)];
    [_scroll setBackgroundColor:[UIColor clearColor]];
    [_scroll setPagingEnabled:YES];
    [_scroll setBounces:NO];
    [_scroll setScrollEnabled:YES];
    [_scroll setShowsHorizontalScrollIndicator:NO];
    [_scroll setShowsVerticalScrollIndicator:NO];
    [_scroll setDelegate:self];
    [_scroll setContentSize:CGSizeMake(375*3, screenHeight-160)];
    [self.view addSubview:_scroll];
    
    drugPrescribeViewController=[[DrugPrescribeViewController alloc] initWithNibName:@"DrugPrescribeViewController" bundle:[NSBundle mainBundle]];
    drugPrescribeViewController.workOrderObject=workOrderObject;
    drugPrescribeViewController.view.frame=CGRectMake(0,0, screenWidth, screenHeight-80);
    [_scroll addSubview:drugPrescribeViewController.view];
   
    drugPromotedViewController=[[DrugPromotedViewController alloc] initWithNibName:@"DrugPromotedViewController" bundle:[NSBundle mainBundle]];
    drugPromotedViewController.workOrderObject=workOrderObject;
    drugPromotedViewController.view.frame=CGRectMake(screenWidth,0, screenWidth, screenHeight-80);
    [_scroll addSubview:drugPromotedViewController.view];
    
    allDrugViewController=[[AllDrugViewController alloc] initWithNibName:@"AllDrugViewController" bundle:[NSBundle mainBundle]];
    allDrugViewController.workOrderObject=workOrderObject;
    allDrugViewController.view.frame=CGRectMake(screenWidth*2,0, screenWidth, screenHeight-80);
    [_scroll addSubview:allDrugViewController.view];
    
    
}

- (IBAction)firstButtonAction:(id)sender {
    
    int buttonTag=(int)[sender tag];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    [_scroll setContentOffset:CGPointMake(buttonTag*screenWidth, _scroll.contentOffset.y) animated:YES];
    
    switch (buttonTag) {
        case 0:
            [thirdImageView setHidden:YES];
            [selectedImageView setHidden:NO];
            [secondImageView setHidden:YES];
            
            break;
        case 1:
            [thirdImageView setHidden:YES];
            [selectedImageView setHidden:YES];
            [secondImageView setHidden:NO];
            break;
        case 2:
            [thirdImageView setHidden:NO];
            [selectedImageView setHidden:YES];
            [secondImageView setHidden:YES];
            break;
            
        default:
            break;
    }
}
#pragma mark - Scroll View Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offSet= scrollView.contentOffset.x;
    if (offSet<320) {
        [thirdImageView setHidden:YES];
        [selectedImageView setHidden:NO];
        [secondImageView setHidden:YES];
        
        
    }
    else if (offSet<640) {
        [thirdImageView setHidden:YES];
        [selectedImageView setHidden:YES];
        [secondImageView setHidden:NO];
    }
    else
    {
        [thirdImageView setHidden:NO];
        [selectedImageView setHidden:YES];
        [secondImageView setHidden:YES];
    }
    
}

- (IBAction)submitBtnAction:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Other Information"
                              message:nil
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"OK", nil];
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    
    [alertView show];
    
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UITextField *textfield =  [alertView textFieldAtIndex: 0];
    NSString * getOtherInformation=textfield.text;
    if (buttonIndex ==1) {
        WorkOrderManager * workOrderManager=[[WorkOrderManager alloc] init];
        [workOrderObject setExtraInfo:getOtherInformation];
        [workOrderObject setStatus:[NSNumber numberWithInt:50]];
        [workOrderManager updateWorkOrder:workOrderObject];
       
        DashboardViewController * dashboardView=[[DashboardViewController alloc] init];
        VisitsViewController * visitView=[[VisitsViewController alloc] init];
        DoctorsViewController * doctorView=[[DoctorsViewController alloc] init];
        CalenderViewController * calenderView=[[CalenderViewController alloc] init];
        WallViewController * wallView=[[WallViewController alloc] init];
        SettingViewController * settingView=[[SettingViewController alloc] init];
        SalesViewController * salesView=[[SalesViewController alloc] init];
        AdminViewController * adminView=[[AdminViewController alloc] init];
        LogoutViewController * logoutView=[[LogoutViewController alloc] init];
        //    RootController *menuController = [[RootController alloc]
        //                                      initWithViewControllers:@[dashboardView]
        //                                      andMenuTitles:@[@"Duties",]];
        GoToVIsitViewController * gotoVisit=[[GoToVIsitViewController alloc] init];
        
        RootController *menuController = [[RootController alloc]
                                          initWithViewControllers:@[dashboardView,visitView,doctorView,calenderView,wallView,settingView,salesView,adminView,logoutView,gotoVisit]
                                          andMenuTitles:@[@"Duties",@"Home",@"Visits",@"Doctors",@"Calender",@"Wall",@"Syncronize",@"Sales",@"Logout",] andCurrentPage:1];
        self.navigationController.navigationBar.hidden = YES;
        [self.navigationController pushViewController:menuController animated:YES];
    }
   
}
@end
