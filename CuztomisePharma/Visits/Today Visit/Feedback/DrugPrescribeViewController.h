//
//  DrugPrescribeViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 15/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"
@class WorkOrderManager;
@interface DrugPrescribeViewController : UIViewController<UITableViewDataSource,UITableViewDataSource,UIActionSheetDelegate>
{
    NSMutableArray * drugArray;
    IBOutlet UITableView *prescribeTableView;
    WorkOrderManager * workOrderManager;
}
@property (nonatomic, strong)WorkOrder * workOrderObject;
- (IBAction)submitButtonAction:(id)sender;

@end
