//
//  DrugPromotedViewController.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 15/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "DrugPromotedViewController.h"

@interface DrugPromotedViewController ()

@end

@implementation DrugPromotedViewController
@synthesize workOrderObject;
- (void)viewDidLoad {
    [super viewDidLoad];
    drugArray=[[NSMutableArray alloc] init];
    selectedDrugName=[[NSMutableString alloc] init];
    NSString * drugPromoted=workOrderObject.drugPromoted;
    if ([drugPromoted rangeOfString:@","].location != NSNotFound) {
        //Yes found
        NSArray * allDrugArray =[drugPromoted componentsSeparatedByString:@","];
        for (NSString * drugString in allDrugArray)
        {
            [drugArray addObject:drugString];
            
        }
    }
    else
    {
        [drugArray addObject:drugPromoted];
        
    }
    [drugPromotedTableView setBackgroundView:nil];
    [drugPromotedTableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"appBackGround.png"]] ];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [drugArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0) {
        return 0;
    }
    else
        return 15; // you can have your own choice, of course
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"DrugTableViewCell";
    
    DrugTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        NSArray * nibArray=[[NSBundle mainBundle] loadNibNamed:@"DrugTableViewCell" owner:self options:nil];
        cell = [nibArray objectAtIndex:0];
    }
    cell.cellLabelText.text=[drugArray objectAtIndex:indexPath.section];
    [cell.cellSwitchControl setHidden:YES];
   cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"back-1x.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]; 
    cell.cellLabelText.font=[UIFont appNormalFont];

    //
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedDrugName=[drugArray objectAtIndex:indexPath.section];
    UIActionSheet *tempSheet = [[UIActionSheet alloc] initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Ready to Prescribe",@"Sample given",@"Un Interested",nil];
    [tempSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [tempSheet showInView:self.view];
    [tempSheet setTag:indexPath.row];
}
- (void)actionSheet:(UIActionSheet *)theActionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"Button Index %ld",(long)buttonIndex);
    int tag=(int)theActionSheet.tag;
    NSLog(@"Tag %ld",(long)tag);
    switch (buttonIndex) {
        case 0:
        {
            NSString * drugPromoted= workOrderObject.drugPrescribed;
            if ([drugPromoted rangeOfString:@","].location != NSNotFound) {
                NSArray * drugAllArray =[drugPromoted componentsSeparatedByString:@","];
                int i=0;
                for (NSString * drugString in drugAllArray) {
                    NSLog(@"%@",drugString);
                    if ([drugString isEqualToString:selectedDrugName]) {
                        i++;
                        break;
                    }
                }
                if (i==0) {
                    workOrderObject.drugPrescribed=[NSString stringWithFormat:@"%@,%@",workOrderObject.drugPrescribed,selectedDrugName];
                }
            }
            else
            {
                if ([workOrderObject.drugPrescribed isEqualToString:selectedDrugName]) {
                    
                }
                else
                {
                    if (workOrderObject.drugPrescribed.length>0) {
                        workOrderObject.drugPrescribed=[NSString stringWithFormat:@"%@,%@",workOrderObject.drugPrescribed,selectedDrugName];
                        
                    }
                    else
                    {
                        workOrderObject.drugPrescribed=selectedDrugName;
                    }
                }
            }
            
            if ([workOrderObject.drugPromoted containsString:selectedDrugName]) {
                NSString * drugPrescride=[[NSString alloc] init];
                if ([workOrderObject.drugPromoted rangeOfString:@","].location != NSNotFound)
                {
                    NSArray * drugAllArray =[workOrderObject.drugPromoted componentsSeparatedByString:@","];
                    
                    for (  NSString * drug in drugAllArray) {
                        if (![drug isEqualToString:selectedDrugName]) {
                            if (drugPrescride.length>0) {
                                drugPrescride=[drugPrescride stringByAppendingString:[NSString stringWithFormat:@",%@",drug]];
                            }
                            else
                            {
                                drugPrescride=[drugPrescride stringByAppendingString:[NSString stringWithFormat:@"%@",drug]];
                            }
                        }
                    }
                    
                }
                workOrderObject.drugPromoted=drugPrescride;
                
            }
            
            if ([workOrderObject.drugSuggest containsString:selectedDrugName]) {
                NSString * drugPrescride=[[NSString alloc] init];
                if ([workOrderObject.drugSuggest rangeOfString:@","].location != NSNotFound)
                {
                    NSArray * drugAllArray =[workOrderObject.drugSuggest componentsSeparatedByString:@","];
                    
                    for (  NSString * drug in drugAllArray) {
                        if (![drug isEqualToString:selectedDrugName]) {
                            if (drugPrescride.length>0) {
                                drugPrescride=[drugPrescride stringByAppendingString:[NSString stringWithFormat:@",%@",drug]];
                            }
                            else
                            {
                                drugPrescride=[drugPrescride stringByAppendingString:[NSString stringWithFormat:@"%@",drug]];
                            }
                        }
                    }
                    
                }
                workOrderObject.drugSuggest=drugPrescride;
                
            }
        }
        break;
        case 1:
        {
            NSString * drugPromoted= workOrderObject.drugPromoted;
            if ([drugPromoted rangeOfString:@","].location != NSNotFound) {
                NSArray * drugAllArray =[drugPromoted componentsSeparatedByString:@","];
                int i=0;
                for (NSString * drugString in drugAllArray) {
                    NSLog(@"%@",drugString);
                    if ([drugString isEqualToString:selectedDrugName]) {
                        i++;
                        break;
                    }
                }
                if (i==0) {
                    workOrderObject.drugPromoted=[NSString stringWithFormat:@"%@,%@",workOrderObject.drugPromoted,selectedDrugName];
                }
            }
            else
            {
                
                if ([workOrderObject.drugPromoted isEqualToString:selectedDrugName]) {
                    
                }
                else
                {
                    if (workOrderObject.drugPromoted.length>0) {
                        workOrderObject.drugPromoted=[NSString stringWithFormat:@"%@,%@",workOrderObject.drugPromoted,selectedDrugName];
                        
                    }
                    else
                    {
                        workOrderObject.drugPromoted=selectedDrugName;
                    }
                }
            }
            
            if ([workOrderObject.drugPrescribed containsString:selectedDrugName]) {
                NSString * drugPrescride=[[NSString alloc] init];
                if ([workOrderObject.drugPrescribed rangeOfString:@","].location != NSNotFound)
                {
                    NSArray * drugAllArray =[workOrderObject.drugPrescribed componentsSeparatedByString:@","];
                    
                    for (  NSString * drug in drugAllArray) {
                        if (![drug isEqualToString:selectedDrugName]) {
                            if (drugPrescride.length>0) {
                                drugPrescride=[drugPrescride stringByAppendingString:[NSString stringWithFormat:@",%@",drug]];
                            }
                            else
                            {
                                drugPrescride=[drugPrescride stringByAppendingString:[NSString stringWithFormat:@"%@",drug]];
                            }
                        }
                    }
                    
                }
                workOrderObject.drugPrescribed=drugPrescride;
                
            }
            
            if ([workOrderObject.drugSuggest containsString:selectedDrugName]) {
                NSString * drugPrescride=[[NSString alloc] init];
                if ([workOrderObject.drugSuggest rangeOfString:@","].location != NSNotFound)
                {
                    NSArray * drugAllArray =[workOrderObject.drugSuggest componentsSeparatedByString:@","];
                    
                    for (  NSString * drug in drugAllArray) {
                        if (![drug isEqualToString:selectedDrugName]) {
                            if (drugPrescride.length>0) {
                                drugPrescride=[drugPrescride stringByAppendingString:[NSString stringWithFormat:@",%@",drug]];
                            }
                            else
                            {
                                drugPrescride=[drugPrescride stringByAppendingString:[NSString stringWithFormat:@"%@",drug]];
                            }
                        }
                    }
                    
                }
                workOrderObject.drugSuggest=drugPrescride;
                
            }
        }
        break;
            case 2:
        {
            
            
            NSString * drugPromoted= workOrderObject.drugSuggest;
            if ([drugPromoted rangeOfString:@","].location != NSNotFound) {
                NSArray * drugAllArray =[drugPromoted componentsSeparatedByString:@","];
                int i=0;
                for (NSString * drugString in drugAllArray) {
                    NSLog(@"%@",drugString);
                    if ([drugString isEqualToString:selectedDrugName]) {
                        i++;
                        break;
                    }
                }
                if (i==0) {
                    workOrderObject.drugSuggest=[NSString stringWithFormat:@"%@,%@",workOrderObject.drugSuggest,selectedDrugName];
                }
            }
            else
            {
                
                if ([workOrderObject.drugSuggest isEqualToString:selectedDrugName]) {
                    
                }
                else
                {
                    if (workOrderObject.drugSuggest.length>0) {
                        workOrderObject.drugSuggest=[NSString stringWithFormat:@"%@,%@",workOrderObject.drugSuggest,selectedDrugName];
                        
                    }
                    else
                    {
                        workOrderObject.drugSuggest=selectedDrugName;
                    }
                }
            }
            
            if ([workOrderObject.drugPrescribed containsString:selectedDrugName]) {
                NSString * drugPrescride=[[NSString alloc] init];
                if ([workOrderObject.drugPrescribed rangeOfString:@","].location != NSNotFound)
                {
                    NSArray * drugAllArray =[workOrderObject.drugPrescribed componentsSeparatedByString:@","];
                    
                    for (  NSString * drug in drugAllArray) {
                        if (![drug isEqualToString:selectedDrugName]) {
                            if (drugPrescride.length>0) {
                                drugPrescride=[drugPrescride stringByAppendingString:[NSString stringWithFormat:@",%@",drug]];
                            }
                            else
                            {
                                drugPrescride=[drugPrescride stringByAppendingString:[NSString stringWithFormat:@"%@",drug]];
                            }
                        }
                    }
                    
                }
                workOrderObject.drugPrescribed=drugPrescride;
                
            }
            if ([workOrderObject.drugPromoted containsString:selectedDrugName]) {
                NSString * drugPrescride=[[NSString alloc] init];
                if ([workOrderObject.drugPromoted rangeOfString:@","].location != NSNotFound)
                {
                    NSArray * drugAllArray =[workOrderObject.drugPromoted componentsSeparatedByString:@","];
                    
                    for (  NSString * drug in drugAllArray) {
                        if (![drug isEqualToString:selectedDrugName]) {
                            if (drugPrescride.length>0) {
                                drugPrescride=[drugPrescride stringByAppendingString:[NSString stringWithFormat:@",%@",drug]];
                            }
                            else
                            {
                                drugPrescride=[drugPrescride stringByAppendingString:[NSString stringWithFormat:@"%@",drug]];
                            }
                        }
                    }
                    
                }
                workOrderObject.drugPromoted=drugPrescride;
                
            }
        }
            break;
        default:
            break;
    }
    
   
    
    //    if (buttonIndex==1) {
    
    //    }
}

@end
