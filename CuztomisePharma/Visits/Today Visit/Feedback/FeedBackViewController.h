//
//  FeedBackViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 15/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"
#import "DrugPrescribeViewController.h"
#import "DrugPromotedViewController.h"
#import "AllDrugViewController.h"
@class DrugPrescribeViewController;
@class DrugPromotedViewController;
@class AllDrugViewController;

@interface FeedBackViewController : UIViewController<UIScrollViewDelegate,UIAlertViewDelegate>
{
    DrugPrescribeViewController * drugPrescribeViewController;
    DrugPromotedViewController * drugPromotedViewController;
    AllDrugViewController* allDrugViewController;
    UIImageView * secondImageView;
    UIImageView * selectedImageView;
    UIImageView * thirdImageView;
}
- (IBAction)submitBtnAction:(id)sender;
@property (nonatomic, strong) UIScrollView *scroll;

@property (nonatomic, strong)WorkOrder * workOrderObject;
@end
