//
//  DrugTableViewCell.h
//  CuztomisePharma
//
//  Created by Abhishek Batra on 15/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrugTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *cellLabelText;
@property (strong, nonatomic) IBOutlet UISwitch *cellSwitchControl;

@end
