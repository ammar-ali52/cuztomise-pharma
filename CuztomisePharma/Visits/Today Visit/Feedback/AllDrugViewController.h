//
//  AllDrugViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 15/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"

@interface AllDrugViewController : UIViewController<UITableViewDelegate , UITableViewDataSource,UIActionSheetDelegate>
{
    NSMutableArray * drugArray;
    IBOutlet UITableView *allDrugTableView;
    NSMutableString * selectedDrugName;

}
@property (nonatomic, strong)WorkOrder * workOrderObject;

@end
