//
//  RowClickPopoverViewController.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 28/03/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "RowClickPopoverViewController.h"

@interface RowClickPopoverViewController ()

@end

@implementation RowClickPopoverViewController
@synthesize workOrderObject;
- (void)viewDidLoad {
    [super viewDidLoad];
    [goToVisitButton setBackgroundColor:[UIColor  appGreenColor]];
    [goToVisitButton setTitle:@"Go To Visit" forState:UIControlStateNormal];
    [goToVisitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [goToVisitButton setTag:0];
    goToVisitButton.titleLabel.font = [UIFont appNormalFont];
    
    [skipVisitButton setBackgroundColor:[UIColor  appGreenColor]];
    [skipVisitButton setTitle:@"Skip Visit" forState:UIControlStateNormal];
    [skipVisitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [skipVisitButton setTag:1];
    skipVisitButton.titleLabel.font = [UIFont appNormalFont];
    NSLog(@"%@",workOrderObject);
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)goToVisitAction:(id)sender {
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"Screen",workOrderObject,@"workOrder", nil ];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveModelView" object:userInfo];
    
}

- (IBAction)skipVisitAction:(id)sender {
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"Screen",workOrderObject,@"workOrder", nil ];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveModelView" object:userInfo];
    
}
@end
