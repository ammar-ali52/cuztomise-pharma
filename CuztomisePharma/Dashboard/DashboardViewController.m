//
//  DashboardViewController.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 22/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import "DashboardViewController.h"

@interface DashboardViewController ()

@end

@implementation DashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    imageNameArray =[[NSMutableArray alloc] initWithObjects:@"visit-tile.png",@"doctor-tile.png",@"wail-tile.png",@"calendar-tile.png",@"setting-tile.png",@"sale-tile.png",@"admin-tile.png",@"visit.png",@"visit.png",@"visit.png",@"visit.png", nil];
    cellNameArray =[[NSMutableArray alloc] initWithObjects:@"Visits",@"Doctors",@"Wall",@"Calender",@"Settings",@"Sales",@"Admin",@"visit.png",@"visit.png",@"visit.png",@"visit.png", nil];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    _collectionView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    [_collectionView registerClass:[DashboardCollectionViewCell class] forCellWithReuseIdentifier:@"DashboardCollectionViewCell"];
//    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"appBackGround.png"]];
    
    [self.view addSubview:_collectionView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startActivityIndicator:) name:@"StartActivityIndicator" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopActivityIndicator:) name:@"StopActivityIndicator" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadCollectionView:) name:@"UpdateDashboardCell" object:nil];
    // Do any additional setup after loading the view from its nib.
}
-(void)startActivityIndicator:(NSNotification* )note
{
      [activityIndicator startAnimating];
}
-(void)stopActivityIndicator:(NSNotification* )note
{
    dispatch_async(dispatch_get_main_queue(), ^{
        activityIndicator.hidden = YES;
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
    });
//       [activityIndicator stopAnimating];
//        [activityIndicator removeFromSuperview];

}
-(void)loadCollectionView:(NSNotification* )note
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_collectionView setDataSource:self];
        [_collectionView setDelegate:self];
        
        [_collectionView registerClass:[DashboardCollectionViewCell class] forCellWithReuseIdentifier:@"DashboardCollectionViewCell"];
        //    _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"appBackGround.png"]];
        [_collectionView reloadData];
        [_collectionView.collectionViewLayout invalidateLayout];
    });

}
-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"View didappear");
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,0,140,42)];
    titleLabel.text=@"Kapeetus Medicrop";
    titleLabel.textColor=[UIColor appWhiteColor];
    titleLabel.backgroundColor=[UIColor appClearColor];
    self.navigationItem.titleView = titleLabel;
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self navigationItem].leftBarButtonItem = barButton;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// pragma Collection View
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"DashboardCollectionViewCell";
    
    /*  Uncomment this block to use nib-based cells */
    // UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    // UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
    // [titleLabel setText:cellData];
    /* end of nib-based cell block */
    
    /* Uncomment this block to use subclass-based cells */
    
    DashboardCollectionViewCell *cell = (DashboardCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.backgroundColor=[UIColor appGreenColor];
    WorkOrderManager * workOrderManager=[[WorkOrderManager alloc] init];
    NSMutableArray * workOrderArray=[workOrderManager getAllWorkOrderbyStatusofToday:[NSNumber numberWithInt:10] andFilterCity:@""];
    NSLog(@"Cell");
    if (indexPath.row==0) {
        if ([workOrderArray count]>0) {
             cell.tileCountLabel.hidden=NO;
             cell.tileCountLabel.text=[NSString stringWithFormat:@"%lu",(unsigned long)[workOrderArray count] ];
        }
        else
        {
            cell.tileCountLabel.hidden=YES;
   
        }
       

    }
    else
    {
        cell.tileCountLabel.hidden=YES;
    }
    cell.tileImage.image=[UIImage imageNamed:[imageNameArray objectAtIndex:indexPath.row]];
    cell.tileNameLabel.text=[cellNameArray objectAtIndex:indexPath.row];

//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    CGFloat screenWidth = screenRect.size.width;
//    if (screenWidth<350) {
//        [cell setFrame:CGRectMake(0, 0, 120, 120)];
//    }
//    else
//    {
//        [cell setFrame:CGRectMake(0, 0, 140, 140)];
//        
//    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    if (screenWidth<350) {
        return CGSizeMake(140, 140);
    }
    else
    {
        return CGSizeMake(160, 160);

    }
    

//
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    // top, left, bottom, right
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    if (screenWidth<350) {
        return UIEdgeInsetsMake(10,13,5,13);
    }
    else
    {
        return UIEdgeInsetsMake(15,18,15,18);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    DashboardViewController * dashboardView=[[DashboardViewController alloc] init];
    VisitsViewController * visitView=[[VisitsViewController alloc] init];
    DoctorsViewController * doctorView=[[DoctorsViewController alloc] init];
    CalenderViewController * calenderView=[[CalenderViewController alloc] init];
    WallViewController * wallView=[[WallViewController alloc] init];
    SettingViewController * settingView=[[SettingViewController alloc] init];
    SalesViewController * salesView=[[SalesViewController alloc] init];
    AdminViewController * adminView=[[AdminViewController alloc] init];
    LogoutViewController * logoutView=[[LogoutViewController alloc] init];
    //    RootController *menuController = [[RootController alloc]
    //                                      initWithViewControllers:@[dashboardView]
    //                                      andMenuTitles:@[@"Duties",]];
    
    
    RootController *menuController = [[RootController alloc]
                                      initWithViewControllers:@[dashboardView,visitView,doctorView,calenderView,wallView,settingView,salesView,adminView,logoutView,]
                                      andMenuTitles:@[@"Duties",@"Home",@"Visits",@"Doctors",@"Calender",@"Wall",@"Syncronize",@"Sales",@"Logout",] andCurrentPage:(int)indexPath.row+1];
    self.navigationController.navigationBar.hidden = YES;
    [self.navigationController pushViewController:menuController animated:YES];
//    VisitsViewController * visitView=[[VisitsViewController alloc] initWithNibName:@"VisitsViewController" bundle:[NSBundle mainBundle]];
//    [self.navigationController pushViewController:visitView animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
