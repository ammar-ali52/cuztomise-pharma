//
//  DashboardViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 22/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"
#import "RootController.h"


@interface DashboardViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    UICollectionView *_collectionView;
    NSMutableArray * imageNameArray;
    NSMutableArray * cellNameArray;
    UIActivityIndicatorView *activityIndicator;

}
-(void)loadCollectionView;

@end
