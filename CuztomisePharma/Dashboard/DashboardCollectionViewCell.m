//
//  DashboardCollectionViewCell.m
//  CuztomisePharma
//
//  Created by Abhishek Batra on 23/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import "DashboardCollectionViewCell.h"

@implementation DashboardCollectionViewCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"DashboardCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        [_tileCountLabel setTextColor:[UIColor appWhiteColor]];
        [_tileNameLabel setTextColor:[UIColor appWhiteColor]];
        [_tileNameLabel setFont:[UIFont appNormalFont]];
        [_tileCountLabel setFont:[UIFont appMediumFont]];
    }
    
    return self;
    
}

@end
