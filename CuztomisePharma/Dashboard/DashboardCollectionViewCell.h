//
//  DashboardCollectionViewCell.h
//  CuztomisePharma
//
//  Created by Abhishek Batra on 23/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"
@interface DashboardCollectionViewCell : UICollectionViewCell
{
    
}
@property (nonatomic,retain) IBOutlet UIImageView * tileImage;
@property (nonatomic,retain) IBOutlet UILabel *tileNameLabel;
@property (nonatomic,retain) IBOutlet UILabel *tileCountLabel;

@end
