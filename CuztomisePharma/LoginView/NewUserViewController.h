//
//  NewUserViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 19/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"

@interface NewUserViewController : UIViewController<UIAlertViewDelegate,UITextViewDelegate>
{
    IBOutlet UITextField *userNameTextField;
    
    IBOutlet UITextField *userPinTextField;
    IBOutlet UITextField *userEmailId;
    IBOutlet UIButton *signUpButton;
}
- (IBAction)signUpButtonAction:(id)sender;

@end
