//
//  ForgotPinViewController.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 19/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import "ForgotPinViewController.h"

@interface ForgotPinViewController ()

@end

@implementation ForgotPinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    UIButton * backBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"arrow-left.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10,13,20, 30)];
    UIBarButtonItem *backBtnBarButton=[[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:backBtnBarButton,nil];
   
}
- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submitBtnAction:(id)sender {
    
    
    if (userEmailId.text.length>0) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(forgotPasswordServerCall) userInfo:nil repeats:false];
        }
        else
        {
            UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
        }
    
   

   
}
-(void)forgotPasswordServerCall
{
    NSMutableDictionary * userDic=[[NSMutableDictionary alloc]init];
    [userDic setValue:[NSString stringWithFormat:@"%@",userEmailId.text] forKey:@"userEmail"];
    LoginManager * loginManager=[[LoginManager alloc] init];
    NSString * responseString=[loginManager userForgotPassword:userDic];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (responseString==nil && responseString!=nil)
    {
        UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Forgot Password" message:responseString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Forgot Password" message:responseString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    [self.navigationController popViewControllerAnimated:YES];
}
@end
