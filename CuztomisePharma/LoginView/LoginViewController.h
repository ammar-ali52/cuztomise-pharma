//
//  LoginViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 16/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"
#import "RootController.h"
@interface LoginViewController : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField *firstTextField;
    IBOutlet UITextField *thirdTextField;
    IBOutlet UITextField *fourthTextField;
    
    IBOutlet UITextField *fifthTextField;
    IBOutlet UITextField *secondTextField;
    IBOutlet UIButton *loginButton;
    IBOutlet UIButton *newUserButton;
    IBOutlet UIButton *forgotButton;
     dispatch_source_t _timer;
}
- (IBAction)loginButtonAction:(id)sender;
- (IBAction)newUserButtonAction:(id)sender;
- (IBAction)forgotPIN:(id)sender;


@end
