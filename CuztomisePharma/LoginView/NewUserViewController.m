//
//  NewUserViewController.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 19/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import "NewUserViewController.h"

@interface NewUserViewController ()

@end

@implementation NewUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userPinTextField.secureTextEntry = YES;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,0,200,42)];
    titleLabel.text=@"New user? Sign up";
    titleLabel.textColor=[UIColor appWhiteColor];
    titleLabel.backgroundColor=[UIColor appClearColor];
    self.navigationItem.titleView = titleLabel;
    UIButton * backBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"arrow-left.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10,13,20, 30)];
    UIBarButtonItem *backBtnBarButton=[[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:backBtnBarButton,nil];
}
- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)signUpButtonAction:(id)sender {
    NSString * userName=userNameTextField.text;
    NSString * emailId=userEmailId.text;
    NSString * userPin=userPinTextField.text;
    if (userName.length>0 && emailId.length>0 && userPin.length==5) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(signUpServerCall) userInfo:nil repeats:false];
    }
    else
    {
        UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter all field properly with 5 digit in pin field" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    }
}
-(void)signUpServerCall
{
    NSMutableDictionary * signUpDic=[[NSMutableDictionary alloc] init];
    [signUpDic setObject:userNameTextField.text forKey:@"userName"];
    [signUpDic setObject:userEmailId.text forKey:@"emailId"];
    [signUpDic setObject:userPinTextField.text forKey:@"userPin"];
    LoginManager * loginManager=[[LoginManager alloc] init];
    NSString * responseString=[loginManager userSignUp:signUpDic];
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    if ([responseString isEqualToString:aSuccess]) {
        UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Success" message:@"Sign up successful and check your mail" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alertView.tag=1;
        [alertView show];
    }
    else
    {
        UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:responseString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
       [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        //Do something else
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    int length = (int)[currentString length];
    if (length<6) {
        return YES;
    }
    return NO;
}
@end
