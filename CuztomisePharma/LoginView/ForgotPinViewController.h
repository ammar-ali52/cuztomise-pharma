//
//  ForgotPinViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 19/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"

@interface ForgotPinViewController : UIViewController<UIAlertViewDelegate>
{
    
    IBOutlet UITextField *userEmailId;
    IBOutlet UIButton *submitButton;
}
- (IBAction)submitBtnAction:(id)sender;

@end
