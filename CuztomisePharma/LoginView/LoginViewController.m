//
//  LoginViewController.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 16/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import "LoginViewController.h"
dispatch_source_t CreateLocationTimer(double interval, dispatch_queue_t queue, dispatch_block_t block)
{
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    if (timer)
    {
        dispatch_source_set_timer(timer, dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), interval * NSEC_PER_SEC, (1ull * NSEC_PER_SEC) / 10);
        dispatch_source_set_event_handler(timer, block);
        dispatch_resume(timer);
    }
    return timer;
}
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    loginButton.backgroundColor=[UIColor appBlackColor];
    [loginButton setTitleColor:[UIColor appWhiteColor] forState:UIControlStateNormal];
    loginButton.titleLabel.font = [UIFont appExtraLargeFont];
    [newUserButton setTitleColor:[UIColor appBlackColor] forState:UIControlStateNormal];
    newUserButton.titleLabel.font = [UIFont appLargeFont];
    [forgotButton setTitleColor:[UIColor appBlackColor] forState:UIControlStateNormal];
    forgotButton.titleLabel.font = [UIFont appLargeFont];
}



- (IBAction)loginButtonAction:(id)sender {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(loginServerCall) userInfo:nil repeats:false];
    
}
-(void)loginServerCall
{
    
    int length = (int)[firstTextField.text length];
    if (length==5) {
        NSMutableDictionary * userDic=[[NSMutableDictionary alloc]init];
        [userDic setValue:[NSString stringWithFormat:@"%@",firstTextField.text] forKey:@"userPin"];
        LoginManager * loginManager=[[LoginManager alloc] init];
        NSString * returnResponse=[loginManager userLogin:userDic];
        [loginManager sendUserLocationtoServer];
        if ([returnResponse isEqualToString:aSuccessfull]) {
            if (![[Utils getValueFronUserDefault:aFirstTimeSync] isEqualToString:@"1"]) {
                SyncManager * syncManager=[[SyncManager alloc] init];
                [syncManager firstTimeSync];
            }
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            DashboardViewController * dashboardView=[[DashboardViewController alloc] init];
            VisitsViewController * visitView=[[VisitsViewController alloc] init];
            DoctorsViewController * doctorView=[[DoctorsViewController alloc] init];
            CalenderViewController * calenderView=[[CalenderViewController alloc] init];
            WallViewController * wallView=[[WallViewController alloc] init];
            SettingViewController * settingView=[[SettingViewController alloc] init];
            SalesViewController * salesView=[[SalesViewController alloc] init];
            AdminViewController * adminView=[[AdminViewController alloc] init];
            LogoutViewController * logoutView=[[LogoutViewController alloc] init];
            //    RootController *menuController = [[RootController alloc]
            //                                      initWithViewControllers:@[dashboardView]
            //                                      andMenuTitles:@[@"Duties",]];
            
            [self startLocationTimer];

            RootController *menuController = [[RootController alloc]
                                              initWithViewControllers:@[dashboardView,visitView,doctorView,calenderView,wallView,settingView,salesView,adminView,logoutView,]
                                              andMenuTitles:@[@"Duties",@"Home",@"Visits",@"Doctors",@"Calender",@"Wall",@"Syncronize",@"Sales",@"Logout",] andCurrentPage:0];
            
            self.navigationController.navigationBar.hidden = YES;
            [self.navigationController pushViewController:menuController animated:YES];
        }
        else
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:returnResponse delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    else
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter five digit pin." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
   
}


- (IBAction)newUserButtonAction:(id)sender {
    NewUserViewController * newUserView=[[NewUserViewController alloc] initWithNibName:@"NewUserViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:newUserView animated:YES];
}

- (IBAction)forgotPIN:(id)sender {
    ForgotPinViewController * forgotView=[[ForgotPinViewController alloc] initWithNibName:@"ForgotPinViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:forgotView animated:YES];
}
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    return YES;
//}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(loginServerCall) userInfo:nil repeats:false];
    NSLog(@"%@",textField.text);
    return YES;
}
//- (void) textFieldDidBeginEditing:(UITextField *)textField {
//     NSLog(@"%@",textField.text);
//}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    int length = (int)[currentString length];
    if (length<5) {
        return YES;
    }
    else if (length==5)
    {
        firstTextField.text=currentString;
        [textField resignFirstResponder];
        return NO;
    }
    return NO;
}
- (void)startLocationTimer
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    double secondsToFire = 15*60*1.000f;
    
    _timer = CreateLocationTimer(secondsToFire, queue, ^{
         LoginManager * loginManager=[[LoginManager alloc] init];
        [loginManager sendUserLocationtoServer];
        loginManager=nil;
    });
}
@end
