//
//  DoctorTableViewCell.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 17/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *clientNameTextView;
@property (strong, nonatomic) IBOutlet UILabel *clientQualificationTextView;
@property (strong, nonatomic) IBOutlet UILabel *ppdTextView;
@property (strong, nonatomic) IBOutlet UILabel *zoneNameTextView;

@end
