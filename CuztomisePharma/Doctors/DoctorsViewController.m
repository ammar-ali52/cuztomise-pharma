//
//  DoctorsViewController.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 23/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import "DoctorsViewController.h"

@interface DoctorsViewController ()

@end

@implementation DoctorsViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
         doctorArray= [[NSMutableArray alloc] init];
        clientManager = [[ClientManager alloc] init];
        originalArray=[[NSMutableArray alloc] init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,0,140,42)];
    titleLabel.text=@"Doctor";
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor appWhiteColor];
    titleLabel.backgroundColor=[UIColor appClearColor];
    self.navigationItem.titleView = titleLabel;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    doctorArray=[clientManager getAllClient];
    originalArray=doctorArray;
    UIButton * infoBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [infoBtn setImage:[UIImage imageNamed:@"add-tile.png"] forState:UIControlStateNormal];
    [infoBtn addTarget:self action:@selector(addDoctorButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [infoBtn setFrame:CGRectMake(5,10,30, 30)];
    UIBarButtonItem *infoBarButton=[[UIBarButtonItem alloc]initWithCustomView:infoBtn];
//
//    UIButton * syncBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
//    [syncBtn setTitle:@"Sync" forState:UIControlStateNormal];
//    [syncBtn addTarget:self action:@selector(syncDoctorButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//    [syncBtn setFrame:CGRectMake(35,10,60, 30)];
//    UIBarButtonItem *syncBtnBarButton=[[UIBarButtonItem alloc]initWithCustomView:syncBtn];
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:infoBarButton,nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [doctorArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0) {
        return 0;
    }
    else
        return 5; // you can have your own choice, of course
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"DoctorTableViewCell";
    
    DoctorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        NSArray * nibArray=[[NSBundle mainBundle] loadNibNamed:@"DoctorTableViewCell" owner:self options:nil];
        cell = [nibArray objectAtIndex:0];
    }
    Client * clientObject=[doctorArray objectAtIndex:indexPath.section];
    cell.clientNameTextView.text=clientObject.name;
    cell.clientQualificationTextView.text=clientObject.qualification;
    cell.ppdTextView.text=[NSString stringWithFormat:@"PPD: %@",clientObject.pPD];
    cell.zoneNameTextView.text=[NSString stringWithFormat:@"Zone: %@",clientObject.drZone];
    
    
    
    //
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    RowClickPopoverViewController *detailViewController = [[RowClickPopoverViewController alloc] initWithNibName:@"RowClickPopoverViewController" bundle:nil];
//    detailViewController.workOrderObject=[workOrderArray objectAtIndex:indexPath.section];
//    [self presentPopupViewController:detailViewController animationType:(int)indexPath.row];
}
- (IBAction)addDoctorButtonClick:(id)sender
{
    AddDoctorViewController * addDoctorViewController=[[AddDoctorViewController alloc] initWithNibName:@"AddDoctorViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:addDoctorViewController animated:YES];
}
- (IBAction)syncDoctorButtonClick:(id)sender
{
    
}
- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    [theSearchBar resignFirstResponder];
    theSearchBar.showsCancelButton=NO;

    NSString *searchString = theSearchBar.text;
    if (searchString.length>0) {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchString];
        doctorArray =(NSMutableArray *) [originalArray filteredArrayUsingPredicate:resultPredicate];
        [doctorTable reloadData];
    }
    else
    {
        doctorArray=originalArray;
        [doctorTable reloadData];
        
    }
    
    NSLog(@"%@",searchString);
    
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar
{
    theSearchBar.showsCancelButton=YES;
    
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar1
{
    [searchBar1 resignFirstResponder];
    NSString *searchString = searchBar1.text;
     searchBar1.showsCancelButton=NO;
    if (searchString.length>0) {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchString];
        doctorArray =(NSMutableArray *) [originalArray filteredArrayUsingPredicate:resultPredicate];
        [doctorTable reloadData];
    }
    else
    {
        doctorArray=originalArray;
        [doctorTable reloadData];
        
    }
    
}

- (void)searchBar:(UISearchBar *)searchBar1 textDidChange:(NSString *)searchText
{
    if (searchText.length>0) {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
        doctorArray =(NSMutableArray *) [originalArray filteredArrayUsingPredicate:resultPredicate];
        [doctorTable reloadData];
    }
    else
    {
        doctorArray=originalArray;
        [doctorTable reloadData];
        
    }
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar1
{
    [searchBar1 resignFirstResponder];
    NSString *searchString = searchBar1.text;
    if (searchString.length>0) {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchString];
        doctorArray =(NSMutableArray *) [originalArray filteredArrayUsingPredicate:resultPredicate];
        [doctorTable reloadData];
    }
    else
    {
        doctorArray=originalArray;
        [doctorTable reloadData];
        
    }
}
@end
