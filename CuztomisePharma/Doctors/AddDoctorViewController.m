
//
//  AddDoctorViewController.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 17/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "AddDoctorViewController.h"

@interface AddDoctorViewController ()

@end

@implementation AddDoctorViewController

- (void)viewDidLoad {
    [addDoctorButton setEnabled:YES];
    [super viewDidLoad];
    zoneString=[[NSString alloc] init];
    self.navigationController.navigationBar.hidden = NO;
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,0,140,42)];
    titleLabel.text=@"Add Doctor";
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor appWhiteColor];
    titleLabel.backgroundColor=[UIColor appClearColor];
    self.navigationItem.titleView = titleLabel;
    UIButton * backBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"arrow-left.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10,13,20, 30)];
    UIBarButtonItem *backBtnBarButton=[[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:backBtnBarButton,nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField        // return NO to disallow editing.
{
    if (textField==zoneTextField) {
        LoginManager * loginManager=[[LoginManager alloc] init];
        User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
        NSString * zoneName=userObject.zones;
        NSArray * zoneArray=[zoneName componentsSeparatedByString:@","];
        UIActionSheet *tempSheet = [[UIActionSheet alloc] initWithTitle:@"Select City" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:nil];
        for (NSString * zoneName in zoneArray) {
            [tempSheet addButtonWithTitle:zoneName];
        }
        [tempSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
        [tempSheet showInView:self.view];
        loginManager=nil;
         return NO;
    }
    else
    {
        return YES;
    }
   
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (doctorNameTextField==textField) {
        [self.view setFrame:CGRectMake(0,-(50+textField.tag*10),self.view.frame.size.width,self.view.frame.size.height)];
        [doctorNameTextField resignFirstResponder];
        [doctorCityTextField becomeFirstResponder];
        return YES;
    }
    else if (doctorCityTextField==textField) {
        [self.view setFrame:CGRectMake(0,-(50+textField.tag*10),self.view.frame.size.width,self.view.frame.size.height)];
        [textField resignFirstResponder];
        [doctorQualificationTextField becomeFirstResponder];
        return YES;
    }
    else if (doctorQualificationTextField==textField) {
        [self.view setFrame:CGRectMake(0,-(50+textField.tag*10),self.view.frame.size.width,self.view.frame.size.height)];
        [doctorQualificationTextField resignFirstResponder];
        [doctorEmailTextField becomeFirstResponder];
        return YES;
    }
    else if (doctorEmailTextField==textField) {
        [self.view setFrame:CGRectMake(0,-(50+textField.tag*10),self.view.frame.size.width,self.view.frame.size.height)];
        [doctorEmailTextField resignFirstResponder];
        [personCountTextField becomeFirstResponder];
        return YES;
    }
    else if (personCountTextField==textField) {
        [self.view setFrame:CGRectMake(0,-(50+textField.tag*10),self.view.frame.size.width,self.view.frame.size.height)];
        [personCountTextField resignFirstResponder];
        [doctorDOBTextField becomeFirstResponder];
        return YES;
    }
    else if (doctorDOBTextField==textField) {
        [self.view setFrame:CGRectMake(0,-(50+textField.tag*10),self.view.frame.size.width,self.view.frame.size.height)];
        [doctorDOBTextField resignFirstResponder];
        [doctorPhoneTextField becomeFirstResponder];
        return YES;
    }
    else if (doctorPhoneTextField==textField) {
         [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
        [textField resignFirstResponder];
        return YES;
    }
    else
        return YES;
    
    
    
}
- (void)actionSheet:(UIActionSheet *)theActionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [theActionSheet buttonTitleAtIndex:buttonIndex];

    if (![title isEqualToString:@"Cancel"]) {
        zoneTextField.text=title;
        zoneString=title;

    }
    else
        zoneString=@"";
}
- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)addDoctorButtonAction:(id)sender {
    
    NSMutableDictionary * doctorDictionary=[[NSMutableDictionary alloc] init];
    [doctorDictionary setValue:doctorNameTextField.text forKey:@"name"];
    [doctorDictionary setValue:doctorCityTextField.text forKey:@"city"];
    [doctorDictionary setValue:doctorQualificationTextField.text forKey:@"qualification"];
    [doctorDictionary setValue:doctorEmailTextField.text forKey:@"email"];
    [doctorDictionary setValue:personCountTextField.text forKey:@"ppd"];
    [doctorDictionary setValue:doctorDOBTextField.text forKey:@"dob"];
    [doctorDictionary setValue:doctorPhoneTextField.text forKey:@"contactNo"];
    [doctorDictionary setValue:zoneString forKey:@"zone"];
    ClientManager * clientManager=[[ClientManager alloc] init];
    [clientManager saveClient:doctorDictionary];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
