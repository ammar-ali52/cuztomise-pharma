//
//  AddDoctorViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 17/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"

@interface AddDoctorViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate>
{
    IBOutlet UITextField *zoneTextField;
    IBOutlet UITextField *doctorNameTextField;
    IBOutlet UITextField *doctorPhoneTextField;
    IBOutlet UITextField *doctorDOBTextField;
    IBOutlet UITextField *personCountTextField;
    IBOutlet UITextField *doctorEmailTextField;
    IBOutlet UITextField *doctorQualificationTextField;
    IBOutlet UITextField *doctorCityTextField;
    IBOutlet UIButton *addDoctorButton;
    NSString * zoneString;
}
- (IBAction)addDoctorButtonAction:(id)sender;

@end
