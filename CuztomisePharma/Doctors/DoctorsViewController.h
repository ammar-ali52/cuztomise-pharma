//
//  DoctorsViewController.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 23/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"
@class ClientManager;
@interface DoctorsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    NSMutableArray * doctorArray;
    ClientManager * clientManager;
    IBOutlet UISearchBar *searchDoctor;
    NSMutableArray * originalArray;
    IBOutlet UITableView *doctorTable;

}

@end
