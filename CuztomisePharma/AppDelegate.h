//
//  AppDelegate.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 16/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>
#import "LoginViewController.h"
@class LoginViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) NSString* mainUrlString;
@property (nonatomic, retain) NSString * apnsToken;
@property (nonatomic, strong) UINavigationController *navigationController;
@property (nonatomic, strong) LoginViewController *rootViewController;
@property (nonatomic, strong) CLLocationManager * locationManager;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (CLLocationManager *)deviceLocation;


@end

