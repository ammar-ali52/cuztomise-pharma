//
//  ClientManager.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 17/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseImportClass.h"

@interface ClientManager : NSObject
{
    AppDelegate * appDelegate;
}
-(NSMutableArray *)getAllClient;
-(void)saveClient:(NSMutableDictionary *)clientDic;
-(NSMutableArray *)getAllDoctorToSync;
-(void)updateClient:(Client *)clientObject;

@end
