//
//  WorkOrderManager.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 12/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseImportClass.h"

@interface WorkOrderManager : NSObject
{
    AppDelegate * appDelegate;
}
-(NSMutableArray *) getAllWorkOrder;
-(NSMutableArray *) getAllDrug;
-(NSMutableArray *) getAllWorkOrderbyStatus:(NSNumber *)isComplete
                            andFilteredCity:(NSString *)filterCity;
-(NSMutableArray *)getDistinctCity:(NSNumber *)isComplete;
-(void)updateWorkOrder:(WorkOrder *)workOrderDic;
-(NSMutableArray *)getWorkOrderToSyncToServer;
-(void)updateAllWorkOrderToNew;
-(void)completeWorkOrder:(WorkOrder *)workOrderDic;

-(NSMutableArray *) getAllWorkOrderbyStatusofToday:(NSNumber *)isComplete andFilterCity:(NSString *)filterCity;
@end
