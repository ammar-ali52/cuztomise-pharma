//
//  WorkOrderManager.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 12/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "WorkOrderManager.h"

@implementation WorkOrderManager



-(NSMutableArray *) getAllWorkOrder
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"WorkOrder" inManagedObjectContext:appDelegate.managedObjectContext];
    [request setEntity:entity];
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    return mutableFetchResults;
}
-(NSMutableArray *) getAllWorkOrderbyStatus:(NSNumber *)isComplete
                            andFilteredCity:(NSString *)filterCity
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"WorkOrder" inManagedObjectContext:appDelegate.managedObjectContext];
    [request setEntity:entity];
    if ([isComplete intValue]!=2 ) {
        if (filterCity.length>0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"status = %@ and city = '%@'",isComplete,filterCity]];
            [request setPredicate:predicate];
        }
        else
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"status = %@",isComplete]];
            [request setPredicate:predicate];
        }
       
    }
    else
    {
        if (filterCity.length>0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"city = '%@'",filterCity]];
            [request setPredicate:predicate];
        }
      
    }
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    return mutableFetchResults;
}
-(NSMutableArray *) getAllDrug
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Drug" inManagedObjectContext:appDelegate.managedObjectContext];
    [request setEntity:entity];
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    return mutableFetchResults;

}


-(NSMutableArray *)getDistinctCity:(NSNumber *)isComplete
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"WorkOrder"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"WorkOrder" inManagedObjectContext:appDelegate.managedObjectContext];
    
    // Required! Unless you set the resultType to NSDictionaryResultType, distinct can't work.
    // All objects in the backing store are implicitly distinct, but two dictionaries can be duplicates.
    // Since you only want distinct names, only ask for the 'name' property.
    
    switch ([isComplete intValue]) {
        case aNewWorkOrder:
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"status = %@",isComplete]];
            [fetchRequest setPredicate:predicate];

            break;
        }
        case aClosedWorkOrder:
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"status = %@",isComplete]];
            [fetchRequest setPredicate:predicate];

            break;
        }
        case 1:
        {
            break;
        }
            
        default:
            break;
    }
    fetchRequest.resultType = NSDictionaryResultType;
    fetchRequest.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"city"]];
    fetchRequest.returnsDistinctResults = YES;
    
    // Now it should yield an NSArray of distinct values in dictionaries.
    NSMutableArray *dictionaries = (NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    return dictionaries;
}
-(void)updateWorkOrder:(WorkOrder *)workOrderDic;
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [workOrderDic setIsSync:[NSNumber numberWithInt:0]];
    NSError *error = nil;
    if (![appDelegate.managedObjectContext save:&error])
    {
        NSLog(@"ERROR while saving ChangeList - %@", error);
    }

}
-(void)completeWorkOrder:(WorkOrder *)workOrderDic
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSError *error = nil;
    if (![appDelegate.managedObjectContext save:&error])
    {
        NSLog(@"ERROR while saving ChangeList - %@", error);
    }
    
}
-(void)updateAllWorkOrderToNew
{
    NSMutableArray * getAllWorkOrder=[self getAllWorkOrder];
    for (WorkOrder * workOrder in getAllWorkOrder) {
        [workOrder setStatus:[NSNumber numberWithInt:10]];
        [self updateWorkOrder:workOrder];
    }
}
-(NSMutableArray *)getWorkOrderToSyncToServer
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"WorkOrder" inManagedObjectContext:appDelegate.managedObjectContext];
    [request setEntity:entity];
  
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"isSync = 0"]];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    return mutableFetchResults;
}
-(NSMutableArray *) getAllWorkOrderbyStatusofToday:(NSNumber *)isComplete andFilterCity:(NSString *)filterCity
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"WorkOrder" inManagedObjectContext:appDelegate.managedObjectContext];
    [request setEntity:entity];
    if ([isComplete intValue]!=2 ) {
        if (filterCity.length>0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"status = %@ and city = '%@'",isComplete,filterCity]];
            [request setPredicate:predicate];
        }
        else
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"status = %@",isComplete]];
            [request setPredicate:predicate];
        }
        
    }
    else
    {
        if (filterCity.length>0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"city = '%@'",filterCity]];
            [request setPredicate:predicate];
        }
        
    }
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    NSMutableArray * returnArray=[[NSMutableArray alloc] init];
    for (WorkOrder * workOrderObject in mutableFetchResults) {
        NSString * workOrderDate=[Utils dateFromString:workOrderObject.startDate byFormatter:@"dd-MM-yyyy"];
        NSString * todayDate=[Utils dateFromString:[NSDate date]
                                       byFormatter:@"dd-MM-yyyy"];
        if ([workOrderDate  isEqualToString:todayDate]) {
            [returnArray addObject:workOrderObject];
        }
    }
    return returnArray;
}
@end
