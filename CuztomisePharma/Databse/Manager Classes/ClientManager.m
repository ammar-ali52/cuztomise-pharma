//
//  ClientManager.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 17/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "ClientManager.h"

@implementation ClientManager
-(NSMutableArray *)getAllClient
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Client" inManagedObjectContext:appDelegate.managedObjectContext];
    [request setEntity:entity];
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    return mutableFetchResults;
}
-(void)saveClient:(NSMutableDictionary *)clientDic
{
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    Client * clientObject=(Client *)[NSEntityDescription  insertNewObjectForEntityForName:@"Client" inManagedObjectContext:appDelegate.managedObjectContext];
    [clientObject setName:[clientDic objectForKey:@"name"]];
    [clientObject setEmail:[clientDic objectForKey:@"email"]];
    [clientObject setContactNo:[clientDic objectForKey:@"contactNo"]];
    [clientObject setCity:[clientDic objectForKey:@"city"]];
    [clientObject setDob:[Utils stringToDate:[clientDic objectForKey:@"dd-mm-yyyy"] byFormatter:@"dob"]];
    [clientObject setQualification:[clientDic objectForKey:@"qualification"]];
    [clientObject setDrZone:[clientDic objectForKey:@"zone"]];
    [clientObject setPPD:[clientDic objectForKey:@"ppd"]];
    [clientObject setIsSync:[NSNumber numberWithInt:0]];
    NSError *error = nil;
    if (![appDelegate.managedObjectContext save:&error])
    {
        NSLog(@"ERROR while saving ClientList - %@", error);
    }
    
    
}
-(NSMutableArray *)getAllDoctorToSync
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Client" inManagedObjectContext:appDelegate.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"isSync = 0"]];
    [request setPredicate:predicate];
    [request setEntity:entity];
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    return mutableFetchResults;
}
-(void)updateClient:(Client *)clientObject;
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [clientObject setIsSync:[NSNumber numberWithInt:1]];
    NSError *error = nil;
    if (![appDelegate.managedObjectContext save:&error])
    {
        NSLog(@"ERROR while saving ChangeList - %@", error);
    }
    
}
@end
