//
//  LoginManager.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 30/03/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseImportClass.h"
@class AppDelegate;
@interface LoginManager : NSObject
{
    AppDelegate * appDelegate;
}

-(NSString *)userLogin:(NSMutableDictionary *)loginDic;
-(NSString *)userSignUp:(NSMutableDictionary *)signUpDic;
-(void)registerForPushNotification;
-(NSString * )userForgotPassword:(NSMutableDictionary *)forgotDic;
-(User * )getUserByUserId:(NSString *)userId;
-(void)sendUserLocationtoServer;
@end
