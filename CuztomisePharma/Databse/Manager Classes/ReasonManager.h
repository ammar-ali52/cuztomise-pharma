//
//  ReasonManager.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 12/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseImportClass.h"

@interface ReasonManager : NSObject
{
    AppDelegate * appDelegate;
}
-(NSMutableArray *) getAllReason;

@end
