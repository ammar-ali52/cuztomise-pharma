//
//  SyncManager.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 04/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "SyncManager.h"

@implementation SyncManager
-(NSString *) syncServerToApp
{
    NSString * changeLogResponse=[self callForChangeLog];
    if ([changeLogResponse isEqualToString:aSuccessfull]) {
        [self callForUpdateRadius];
        [self callForUpdateReason];
        [self callForUpdateDrug];
        
        [self syncApptoServer];
    }
//    [self syncApptoServer];
    return changeLogResponse;

}
-(void)firstTimeSync
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSData * returnData;
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
  
    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
    LoginManager * loginManager=[[LoginManager alloc] init];
    User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"mobileapp/Firsttimeloginsync/format/json?&time=%@&dbname=%@&zone_id=%@&key=%@",todayDate,userObject.dbName,userObject.zoneId,aServerKey]andRequestType:@"GET"];
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil)
        {
            [Utils setValueToUserDefault:[responeParseDic objectForKey:@"radius"] andKey:aRadius];
            NSMutableArray * dragArray=[responeParseDic objectForKey:@"drug"];
            for (NSMutableDictionary * drugDic in dragArray)
            {
                [self saveDrug:drugDic];
            }
            NSMutableArray * clientArray=[responeParseDic objectForKey:@"client"];
            for (NSMutableDictionary * clientDic in clientArray) {
                [self saveClient:clientDic];
                
            }
            NSMutableArray * reasonArray=[responeParseDic objectForKey:@"reason"];
            for (NSMutableDictionary * reasonDic in reasonArray) {
                [self saveReason:reasonDic];
                
            }
            [Utils setValueToUserDefault:@"1" andKey:aFirstTimeSync];

        }
    }
    
}
-(void)saveClient:(NSMutableDictionary *)clientDic
{
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    Client * clientObject=[self getClientByClientId:[clientDic objectForKey:@"id"]];
    if (clientObject==Nil) {
        clientObject= (Client *)[NSEntityDescription  insertNewObjectForEntityForName:@"Client" inManagedObjectContext:appDelegate.managedObjectContext];
    }
    [clientObject setClientId:[NSNumber numberWithInt:[[clientDic objectForKey:@"id"] intValue]]];
    [clientObject setName:[clientDic objectForKey:@"name"]];
    [clientObject setCompanyName:[clientDic objectForKey:@"company_name"]];
    [clientObject setEmail:[clientDic objectForKey:@"email"]];
    [clientObject setIsActive:[NSNumber numberWithInt:[[clientDic objectForKey:@"is_active"] intValue]]];
    [clientObject setContactNo:[clientDic objectForKey:@"contact_no"]];
    [clientObject setState:[clientDic objectForKey:@"state"]];
    [clientObject setCity:[clientDic objectForKey:@"city"]];
    [clientObject setClientNo:[NSNumber numberWithInt:[[clientDic objectForKey:@"client_no"] intValue]]];
    [clientObject setUserId:[NSNumber numberWithInt:[[clientDic objectForKey:@"userid"] intValue]]];
    [clientObject setAuthCode:[clientDic objectForKey:@"authcode"]];
    [clientObject setQualification:[clientDic objectForKey:@"qualification"]];
    [clientObject setSpeciality:[clientDic objectForKey:@"speciality"]];
    [clientObject setPrefTime:[clientDic objectForKey:@"pref_time"]];
    [clientObject setAlias:[clientDic objectForKey:@"alias"]];
    [clientObject setImagePath:[clientDic objectForKey:@"image_path"]];
    [clientObject setDuration:[clientDic objectForKey:@"duration"]];
    [clientObject setEmployeeAssign:[clientDic objectForKey:@"employee_assigned"]];
    [clientObject setAvgTime:[clientDic objectForKey:@"avg_time"]];
    [clientObject setNotes:[clientDic objectForKey:@"notes"]];
    [clientObject setPrefDay:[clientDic objectForKey:@"pref_day"]];
    [clientObject setZoneId:[clientDic objectForKey:@"zone_ids"]];
    [clientObject setDrZone:[clientDic objectForKey:@"zone"]];
    [clientObject setDoctorCode:[clientDic objectForKey:@"doctor_code"]];
    [clientObject setPPD:[clientDic objectForKey:@"ppd"]];
    if ([clientDic objectForKey:@"category_id"] != (id)[NSNull null] )
    {
        [clientObject setCategoryId:[clientDic objectForKey:@"category_id"]];

    }
    [clientObject setDrugSuggest:[clientDic objectForKey:@"drug_suggest"]];
    [clientObject setDrugPrescribed:[clientDic objectForKey:@"drug_prescribed"]];
    [clientObject setDrugPromoted:[clientDic objectForKey:@"drug_promoted"]];
    [clientObject setIsSync:[NSNumber numberWithInt:1]];
    
    NSError *error = nil;
    
    if (![appDelegate.managedObjectContext save:&error])
    {
        NSLog(@"ERROR while saving ClientList - %@", error);
    }
    
    
}
-(void)saveDrug:(NSMutableDictionary *)drugDic{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    Drug * drugObject=[self getDrugByDrugId:[drugDic objectForKey:@"id"]];
    if (drugObject==Nil) {
        drugObject= (Drug *)[NSEntityDescription  insertNewObjectForEntityForName:@"Drug" inManagedObjectContext:appDelegate.managedObjectContext];
    }
    [drugObject setDrugId:[NSNumber numberWithInt:[[drugDic objectForKey:@"id"] intValue]]];
    [drugObject setDrugName:[drugDic objectForKey:@"drug_name"]];
    [drugObject setDrugCode:[drugDic objectForKey:@"drug_code"]];
    [drugObject setAddedBy:[NSNumber numberWithInt:[[drugDic objectForKey:@"added_by"] intValue]]];
    [drugObject setDivisionId:[NSNumber numberWithInt:[[drugDic objectForKey:@"division_id"] intValue]]];
    
    NSError *error = nil;
    
    if (![appDelegate.managedObjectContext save:&error])
    {
        NSLog(@"ERROR while saving DrugList - %@", error);
    }
//    date_added0000-00-00

}
-(void)saveReason:(NSMutableDictionary *)reasonDic
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    Reason * reasonObject=[self getReasonByReasonId:[reasonDic objectForKey:@"id"]];
    if (reasonObject==Nil) {
        reasonObject= (Reason *)[NSEntityDescription  insertNewObjectForEntityForName:@"Reason" inManagedObjectContext:appDelegate.managedObjectContext];
    }
    [reasonObject setReasonId:[NSNumber numberWithInt:[[reasonDic objectForKey:@"id"] intValue]]];
    [reasonObject setReason:[reasonDic objectForKey:@"reason"]];
    [reasonObject setAddedBy:[NSNumber numberWithInt:[[reasonDic objectForKey:@"added_by"] intValue]]];
    NSError *error = nil;
    
    if (![appDelegate.managedObjectContext save:&error])
    {
        NSLog(@"ERROR while saving DrugList - %@", error);
    }
//    date_created2015-01-14 18:54:46
}
-(Client *)getClientByClientId:(NSString *)clientId
{
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Client" inManagedObjectContext:appDelegate.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"clientId = %@",clientId]];
    [request setPredicate:predicate];
    [request setEntity:entity];
    
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    
    if (mutableFetchResults!=nil && [mutableFetchResults count]>0)
    {
        return [mutableFetchResults objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}
-(Reason *)getReasonByReasonId:(NSString *)reasonId
{
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Reason" inManagedObjectContext:appDelegate.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"reasonId = %@",reasonId]];
    [request setPredicate:predicate];
    [request setEntity:entity];
    
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    
    if (mutableFetchResults!=nil && [mutableFetchResults count]>0)
    {
        return [mutableFetchResults objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}
-(Drug *)getDrugByDrugId:(NSString *)drugId
{
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Drug" inManagedObjectContext:appDelegate.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"drugId = %@",drugId]];
    [request setPredicate:predicate];
    [request setEntity:entity];
    
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    
    if (mutableFetchResults!=nil && [mutableFetchResults count]>0)
    {
        return [mutableFetchResults objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}
-(NSString *)callForChangeLog
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSData * returnData;
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
    LoginManager * loginManager=[[LoginManager alloc] init];
    User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"workorder/fetchChangeLog/format/json?&time=%@&dbname=%@&user_id=%@&key=%@",todayDate,userObject.dbName,userObject.userId,aServerKey]andRequestType:@"GET"];
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil)
        {
            NSMutableArray * resultArray=[responeParseDic objectForKey:@"results"];
            for (  NSMutableDictionary * changeDic in resultArray) {
                NSString * changeLogAction=[changeDic objectForKey:@"action"];
                if ([changeLogAction isEqualToString:@"WA"]||[changeLogAction isEqualToString:@"WU"]) {
                    [self getWorkOrderInfoFromServer:[changeDic objectForKey:@"id"]];

                }
                else if ([changeLogAction isEqualToString:@"WD"])
                {
                    [self deleteWorkOrder:[changeDic objectForKey:@"id"]];
                }
                else if ([changeLogAction isEqualToString:@"DA"])
                {
                    [self getAddedDoctorFromServer:[changeDic objectForKey:@"id"]];
                }
            }
            return aSuccessfull;
        }
        else
            
        {
            return aParsingError;
        }
    }
    
    else
    {
        return  aInternetError;
    }
}
-(void)getAddedDoctorFromServer:(NSString *)changeLogId
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSData * returnData;
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
    LoginManager * loginManager=[[LoginManager alloc] init];
    User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    http://184.106.222.195/cuztomise_api/branches/pharma_api/client/fetchClientbyId/
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"client/fetchClientbyId/format/json?&time=%@&dbname=%@&id=%@&key=%@",todayDate,userObject.dbName, changeLogId,aServerKey]andRequestType:@"GET"];
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil)
        {
            [self saveClient:responeParseDic];
        }
    }

}
//-(void)saveChangeLog:(NSMutableDictionary *)changeDic
//{
//    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    ChangeLog * changeObject=[self getChangeLogObjectById:[changeDic objectForKey:@"id"]];
//    if (changeObject==Nil) {
//        changeObject= (ChangeLog *)[NSEntityDescription  insertNewObjectForEntityForName:@"ChangeLog" inManagedObjectContext:appDelegate.managedObjectContext];
//    }
//    [changeObject setChangeId:[NSNumber numberWithInt:[[changeDic objectForKey:@"id"] intValue]]];
//    [changeObject setUserId:[NSNumber numberWithInt:[[changeDic objectForKey:@"user_id"] intValue]]];
//    [changeObject setIsSync:[NSNumber numberWithInt:[[changeDic objectForKey:@"is_syn"] intValue]]];
//    [changeObject setIsAck:[NSNumber numberWithInt:[[changeDic objectForKey:@"is_ack"] intValue]]];
//    [changeObject setAction:[changeDic objectForKey:@"action"]];
//    [changeObject setChangeCode:[changeDic objectForKey:@"change_codes"]];
//    [changeObject setTimestamp:[Utils stringToDate:[changeDic objectForKey:@""]
//                                       byFormatter:@"yyyy-mm-dd hh:mm:ss"]];
//    
//    NSError *error = nil;
//    if (![appDelegate.managedObjectContext save:&error])
//    {
//        NSLog(@"ERROR while saving ChangeList - %@", error);
//    }
//    [self getWorkOrderInfoFromServer:[changeDic objectForKey:@"id"]];
//}
-(ChangeLog *)getChangeLogObjectById:(NSString *)changeId
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ChangeLog" inManagedObjectContext:appDelegate.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"changeId = %@",changeId]];
    [request setPredicate:predicate];
    [request setEntity:entity];
    
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    
    if (mutableFetchResults!=nil && [mutableFetchResults count]>0)
    {
        return [mutableFetchResults objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}

-(void)getWorkOrderInfoFromServer:(NSString *)workOrderId
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSData * returnData;
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
    LoginManager * loginManager=[[LoginManager alloc] init];
    User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"workorder/getWOInfoMobile/format/json?&time=%@&dbname=%@&woid=%@&key=%@",todayDate,userObject.dbName, workOrderId,aServerKey]andRequestType:@"GET"];
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil)
        {
            NSMutableArray * resultArray=[responeParseDic objectForKey:@"results"];
            for (  NSMutableDictionary * workOrderDic in resultArray) {
                [self saveWorkOrder:workOrderDic];
            }
        }
    }

}
-(void)deleteWorkOrder:(NSString *)workOrderId
{
    WorkOrder * workOrderObject=[self getWorkOrderbyId:workOrderId];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.managedObjectContext deleteObject:workOrderObject];
    NSError *error = nil;

    if (![appDelegate.managedObjectContext save:&error])
    {
        NSLog(@"ERROR while saving ChangeList - %@", error);
    }
    [self updateChangeLogbyWorkOrderId:workOrderId];

}
-(WorkOrder *) getWorkOrderbyId:(NSString *)workOrderId
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"WorkOrder" inManagedObjectContext:appDelegate.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"workOrderId = %@",workOrderId]];
    [request setPredicate:predicate];
    [request setEntity:entity];
    
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    
    if (mutableFetchResults!=nil && [mutableFetchResults count]>0)
    {
        return [mutableFetchResults objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}
-(void)saveWorkOrder:(NSMutableDictionary *)workOrderDic
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    WorkOrder * workOrderObject=[self getWorkOrderbyId:[workOrderDic objectForKey:@"id"]];
    if (workOrderObject==Nil) {
        workOrderObject= (WorkOrder *)[NSEntityDescription  insertNewObjectForEntityForName:@"WorkOrder" inManagedObjectContext:appDelegate.managedObjectContext];
    }
    [workOrderObject setWorkOrderId:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"id"] intValue]]];
    
    [workOrderObject setOrderNo:[workOrderDic objectForKey:@"order_no"]];
    [workOrderObject setClientNo:[workOrderDic objectForKey:@"client_no"]];
    [workOrderObject setClientId:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"client_id"] intValue]]];
    [workOrderObject setClientAddId:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"client_add_id"] intValue]]];
    [workOrderObject setReceiveDate:[Utils stringToDate:[workOrderDic objectForKey:@"receive_date"] byFormatter:@"yyyy-MM-dd"]];
     [workOrderObject setStartDate:[Utils stringToDate:[workOrderDic objectForKey:@"start_date"] byFormatter:@"yyyy-MM-dd"]];
    [workOrderObject setEndDate:[Utils stringToDate:[workOrderDic objectForKey:@"end_date"] byFormatter:@"yyyy-MM-dd"]];
    [workOrderObject setExtraInfo:[workOrderDic objectForKey:@"extra_info"]];
    [workOrderObject setCreatedBy:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"created_by"] intValue]]];
    [workOrderObject setDateCreated:[Utils stringToDate:[workOrderDic objectForKey:@"date_created"] byFormatter:@"yyyy-mm-dd hh:mm:ss"]];
    
    [workOrderObject setDiscount:[NSNumber numberWithFloat:[[workOrderDic objectForKey:@"discount"] floatValue]]];
    [workOrderObject setTotal:[NSNumber numberWithFloat:[[workOrderDic objectForKey:@"total"] floatValue]]];
    [workOrderObject setGrandTotal:[NSNumber numberWithFloat:[[workOrderDic objectForKey:@"grandtotal"] floatValue]]];
    [workOrderObject setProjectLimitAmt:[NSNumber numberWithFloat:[[workOrderDic objectForKey:@"project_limit_amt"] floatValue]]];
     [workOrderObject setIsInvoiced:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"is_invoiced"] intValue]]];
    [workOrderObject setCpName:[workOrderDic objectForKey:@"cp_name"]];
    [workOrderObject setCpEmail:[workOrderDic objectForKey:@"cp_email"]];
    [workOrderObject setCpPhone:[workOrderDic objectForKey:@"cp_phone"]];
    [workOrderObject setIsNotifySent:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"is_notif_sent"] intValue]]];
    [workOrderObject setSubject:[workOrderDic objectForKey:@"subject"]];
    [workOrderObject setNotificationEmail:[workOrderDic objectForKey:@"notification_email"]];
    [workOrderObject setIsEmpNotify:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"is_emp_notify"] intValue]]];
    [workOrderObject setIsAdminNotify:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"is_admin_notify"] intValue]]];
     [workOrderObject setStatus:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"status"] intValue]]];
       [workOrderObject setDateApproved:[Utils stringToDate:[workOrderDic objectForKey:@"date_approved"] byFormatter:@"yyyy-mm-dd hh:mm:ss"]];
    [workOrderObject setIsHidden:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"is_hidden"] intValue]]];
    [workOrderObject setPriority:[workOrderDic objectForKey:@"priority"]];
    [workOrderObject setEstimatedWorkTime:[workOrderDic objectForKey:@"estimated_work_time"]];
    [workOrderObject setComplexity:[workOrderDic objectForKey:@"complexitylevel"]];
    [workOrderObject setJobType:[workOrderDic objectForKey:@"job_type"]];
    [workOrderObject setBillingType:[workOrderDic objectForKey:@"billing_type"]];
    [workOrderObject setLastsValue:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"lasts_value"] intValue]]];
    [workOrderObject setLastDuaration:[workOrderDic objectForKey:@"lasts_duration"]];
    [workOrderObject setPrefStartTime:[Utils stringToDate:[workOrderDic objectForKey:@"pref_start_time"] byFormatter:@"yyyy-mm-dd hh:mm:ss"]];
     [workOrderObject setScheduleIds:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"if_scheduled"] intValue]]];
    [workOrderObject setIfVisits:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"if_visits"] intValue]]];
    [workOrderObject setWoExtraField:[workOrderDic objectForKey:@"wo_extra_field"]];
    [workOrderObject setInvoicing:[workOrderDic objectForKey:@"invoicing"]];
    [workOrderObject setLocation:[workOrderDic objectForKey:@"location"]];
     [workOrderObject setLatitude:[NSNumber numberWithDouble:[[workOrderDic objectForKey:@"latitude"] doubleValue]]];
     [workOrderObject setLongitude:[NSNumber numberWithDouble:[[workOrderDic objectForKey:@"longitude"] doubleValue]]];
    [workOrderObject setReason:[workOrderDic objectForKey:@"reason"]];

    [workOrderObject setIsComplete:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"is_complete"] intValue]]];
    [workOrderObject setDetail:[workOrderDic objectForKey:@"detail"]];
    [workOrderObject setIsMonthVisit:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"is_month_visit"] intValue]]];
    [workOrderObject setWorkDateTimeCreated:[Utils stringToDate:[workOrderDic objectForKey:@"work_datetime_created"] byFormatter:@"yyyy-mm-dd hh:mm:ss"]];

    [workOrderObject setFeedback:[workOrderDic objectForKey:@"feedback"]];
    [workOrderObject setLastUpdate:[Utils stringToDate:[workOrderDic objectForKey:@"last_updated"] byFormatter:@"yyyy-mm-dd hh:mm:ss"]];
    [workOrderObject setIsReschedule:[NSNumber numberWithInt:[[workOrderDic objectForKey:@"is_reschedule"] intValue]]];
//    [workOrderObject setIsRescheduleTimes:[Utils stringToDate:[workOrderDic objectForKey:@"is_reschedule_times"] byFormatter:@"yyyy-mm-dd hh:mm:ss"]];
    [workOrderObject setIsSync:[NSNumber numberWithInt:1]];
    
    [workOrderObject setMrLatLongFrom:[workOrderDic objectForKey:@"mr_latlon_from"]];
    [workOrderObject setMrLocationFrom:[workOrderDic objectForKey:@"mr_location_from"]];
    [workOrderObject setName:[workOrderDic objectForKey:@"name"]];

    [workOrderObject setEmail:[workOrderDic objectForKey:@"email"]];
    [workOrderObject setCity:[workOrderDic objectForKey:@"city"]];
    [workOrderObject setContactNo:[workOrderDic objectForKey:@"contact_no"]];
    [workOrderObject setZoneName:[workOrderDic objectForKey:@"zone"]];
    [workOrderObject setZoneId:[workOrderDic objectForKey:@"zone_ids"]];
    [workOrderObject setNotes:[workOrderDic objectForKey:@"notes"]];
    [workOrderObject setQualification:[workOrderDic objectForKey:@"qualification"]];
    [workOrderObject setDrugPrescribed:[workOrderDic objectForKey:@"drug_prescribed"]];
    [workOrderObject setDrugPromoted:[workOrderDic objectForKey:@"drug_promoted"]];
    NSError *error = nil;
    if (![appDelegate.managedObjectContext save:&error])
    {
        NSLog(@"ERROR while saving ChangeList - %@", error);
    }
    [self updateChangeLogbyWorkOrderId:[workOrderDic objectForKey:@"id"]];
    
}
-(void)updateChangeLogbyWorkOrderId:(NSString *)workOrderId

{
    NSData * returnData;
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
    LoginManager * loginManager=[[LoginManager alloc] init];
    User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"workorder/updateChangeLog/format/json?&time=%@&dbname=%@&id=%@&key=%@&user_id=%@",todayDate,userObject.dbName, workOrderId,aServerKey,userObject.userId]andRequestType:@"GET"];
    
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil)
        {
            
        }
    }
    loginManager=nil;
    userObject=nil;
}
-(void)callForUpdateRadius
{
    NSData * returnData;
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
    LoginManager * loginManager=[[LoginManager alloc] init];
    User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"setting/fetchGlobalSettings/format/json?&time=%@&dbname=%@&key=%@&vkey=CHECKIN_RADIUS",todayDate,userObject.dbName,aServerKey]andRequestType:@"GET"];
    
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil)
        {
            [Utils setValueToUserDefault:[responeParseDic objectForKey:@"value"] andKey:aRadius];
          
           
        }
    }
    
}
-(void)callForUpdateReason
{
    NSData * returnData;
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
    LoginManager * loginManager=[[LoginManager alloc] init];
    User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"setting/fetchReason/format/json?&time=%@&dbname=%@&key=%@",todayDate,userObject.dbName,aServerKey]andRequestType:@"GET"];
    
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil)
        {
            NSMutableArray * resultArray=[responeParseDic objectForKey:@"results"];
            for (  NSMutableDictionary * reasonDic in resultArray) {
                [self saveReason:reasonDic];
            }
        }
    }
    
    
}
-(void)callForUpdateDrug
{
    NSData * returnData;
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
    LoginManager * loginManager=[[LoginManager alloc] init];
    User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"drug/fetchDrug/format/json?&time=%@&dbname=%@&key=%@",todayDate,userObject.dbName,aServerKey]andRequestType:@"GET"];
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil)
        {
            NSMutableArray * resultArray=[responeParseDic objectForKey:@"results"];
            for (  NSMutableDictionary * drugDic in resultArray) {
                [self saveDrug:drugDic];
            }
        }
    }
}
-(void)syncApptoServer
{
    [self callForWorkOrderSyncToServer];
    [self addDoctorToServer];

}
-(void)callForWorkOrderSyncToServer
{
    NSPredicate *predicate;
    NSArray *filteredArray ;
    WorkOrderManager * workOrderManager=[[WorkOrderManager alloc] init];
    NSMutableArray * workOrderArray=[workOrderManager getWorkOrderToSyncToServer];
    predicate= [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"status = 50"]];
    filteredArray = [workOrderArray filteredArrayUsingPredicate:predicate ];
    for (WorkOrder * workOrderObject in filteredArray)
    {
        [self sendCloseVisitToServer:workOrderObject];
    }
    predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"status = 40"]];
    filteredArray  = [workOrderArray filteredArrayUsingPredicate:predicate ];
    for (WorkOrder * workOrderObject in filteredArray)
    {
        [self sendSkipVisitToServer:workOrderObject];
    }
    predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"status = 30"]];
    filteredArray  = [workOrderArray filteredArrayUsingPredicate:predicate ];
    for (WorkOrder * workOrderObject in filteredArray)
    {
        [self sendRescheduleVisitToServer:workOrderObject];
    }
    
//
    
}
-(void)sendCloseVisitToServer:(WorkOrder *)workOrderObject
{
     // [{"feedback":"[{\"Other Notes\":\"testing with ammar\",\"Quitus-TZ Tab.\":\"no\",\"Was the doctor ready for trial?\":\"Yes\",\"Onrid MD Tab.\":\"yes\"}]","id":"649","empid":"149","checkoutlatlong":"0.0,0.0","checkinlatlong":"0.0,0.0","startdate":"2015-05-01","checkinout":"2015-05-01 09:59:57,2015-05-01 10:00:28","mr_latlon_from":"0.0,0.0","extra_info":"","client_id":"3","cust_id":"60","accompaniedby":"13,3","order_no":"himpharma648"}]
    NSData * returnData;
    LoginManager * loginManager=[[LoginManager alloc] init];
    User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    NSMutableArray * serverSendArray=[[NSMutableArray alloc] init];
    
        NSMutableDictionary * workOrderDic=[[NSMutableDictionary alloc] init];
        [workOrderDic setObject:workOrderObject.workOrderId forKey:@"id"];
        [workOrderDic setObject:userObject.userId forKey:@"empid"];
        [workOrderDic setObject:[NSString stringWithFormat:@"0.0,0.0" ] forKey:@"checkoutlatlong"];
        [workOrderDic setObject:workOrderObject.mrLatLongFrom forKey:@"checkinlatlong"];
    
        [workOrderDic setObject:[NSString stringWithFormat:@"%@,%@",[Utils dateFromString:workOrderObject.dateCreated byFormatter:@"yyyy-MM-dd hh:mm:ss"],[Utils dateFromString:[NSDate date] byFormatter:@"yyyy-MM-dd hh:mm:ss"]] forKey:@"checkinout"];
    
        [workOrderDic setObject:workOrderObject.mrLatLongFrom forKey:@"mr_latlon_from"];
        
          [workOrderDic setObject:[Utils dateFromString:workOrderObject.startDate byFormatter:@"yyyy-MM-dd hh:mm:ss"] forKey:@"startdate"];
        [workOrderDic setObject:workOrderObject.notes forKey:@"extra_info"];
        [workOrderDic setObject:workOrderObject.reason forKey:@"skipreason"];
        [workOrderDic setObject:@"0" forKey:@"accompaniedby"];

        

        [workOrderDic setObject:workOrderObject.clientId forKey:@"client_id"];
        [workOrderDic setObject:workOrderObject.orderNo forKey:@"order_no"];
        [workOrderDic setObject:userObject.userId forKey:@"cust_id"];
        NSMutableDictionary * feedbackDic=[[NSMutableDictionary alloc] init];
        [feedbackDic setObject:workOrderObject.extraInfo forKey:@"Other Notes"];
        [feedbackDic setObject:@"Yes" forKey:@"Was the doctor ready for trial?"];
        if ([workOrderObject.drugPrescribed rangeOfString:@","].location != NSNotFound) {
            //Yes found
            NSArray * allDrugArray =[workOrderObject.drugPrescribed componentsSeparatedByString:@","];
            for (NSString * drugString in allDrugArray)
            {
                    [feedbackDic setValue:@"yes" forKey:drugString];
            }
        }
        else
        {
            if (workOrderObject.drugPrescribed.length>0) {
                [feedbackDic setValue:@"yes" forKey:workOrderObject.drugPrescribed];
                
            }
        }
        if ([workOrderObject.drugPromoted rangeOfString:@","].location != NSNotFound) {
            //Yes found
            NSArray * allDrugArray =[workOrderObject.drugPromoted componentsSeparatedByString:@","];
            for (NSString * drugString in allDrugArray)
            {
               
                    [feedbackDic setValue:@"no" forKey:drugString];
            }
        }
        else
        {
            if (workOrderObject.drugPromoted.length>0) {
                [feedbackDic setValue:@"no" forKey:workOrderObject.drugPromoted];

            }
        }
      
        if ([workOrderObject.drugSuggest rangeOfString:@","].location != NSNotFound) {
            //Yes found
            NSArray * allDrugArray =[workOrderObject.drugSuggest componentsSeparatedByString:@","];
            for (NSString * drugString in allDrugArray)
            {
          
                    [feedbackDic setValue:@"maybe" forKey:drugString];
            }
        }
        else
        {
            if (workOrderObject.drugSuggest.length>0) {
                [feedbackDic setValue:@"maybe" forKey:workOrderObject.drugSuggest];
                
            }
        }
    
        NSMutableArray * feedbackArray=[[NSMutableArray alloc] init];
        [feedbackArray addObject:feedbackDic];

        NSData *feedbackJson = [NSJSONSerialization dataWithJSONObject:feedbackArray
                                                       options:0
                                                         error:nil ];
        
        NSString *feedbackJsonString = [[NSString alloc] initWithData:feedbackJson encoding:NSUTF8StringEncoding];
        
        [workOrderDic setObject:feedbackJsonString forKey:@"feedback"];
        [serverSendArray addObject:workOrderDic];
    
   
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
//    NSDictionary *myDictionary = [NSDictionary dictionaryWithObject:@&quot;Hello&quot; forKey:@&quot;World&quot;];
 
    
    NSData *json = [NSJSONSerialization dataWithJSONObject:serverSendArray
                                                   options:0
                                                     error:nil ];
    
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"workorder/dataSynByMobileApp/format/json?&time=%@&dbname=%@&key=%@&status=50&data=%@",todayDate,userObject.dbName,aServerKey,jsonString]andRequestType:@"GET"];
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil)
        {
            if([responeParseDic valueForKey:@"success"]!=Nil)
            {
                [workOrderObject setIsSync:[NSNumber numberWithInt:1]];
                WorkOrderManager * workOrderManager=[[WorkOrderManager alloc] init];
                
                [workOrderManager completeWorkOrder:workOrderObject];
            }
            
        }
    }
    

}
-(void)sendSkipVisitToServer:(WorkOrder *)workOrderObject
{
    // [dbname=cuztomise_childthief, key=JVP8xGk4hsX2cZd0L3NQwYbI0mf4exPiSoAhVYnz, status=40, data=[{"checkinout":",","visitDate":"2015-05-08","reschreason":"","skipreason":"Doctor on emergency ","cust_id":"60","order_no":"app\/149\/10","id":"app\/149\/10","empid":"149","startdate":"2015-05-08","client_id":"69","skip_resch_time":"2015-05-08 04:46:07"}]]
    NSData * returnData;
    LoginManager * loginManager=[[LoginManager alloc] init];
    User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    NSMutableArray * serverSendArray=[[NSMutableArray alloc] init];
        NSMutableDictionary * workOrderDic=[[NSMutableDictionary alloc] init];
        [workOrderDic setObject:workOrderObject.workOrderId forKey:@"id"];
        [workOrderDic setObject:userObject.userId forKey:@"empid"];
        [workOrderDic setObject:[Utils dateFromString:workOrderObject.startDate byFormatter:@"yyyy-MM-dd hh:mm:ss"] forKey:@"startdate"];
        [workOrderDic setObject:[NSString stringWithFormat:@"%@,%@",[Utils dateFromString:workOrderObject.startDate byFormatter:@"yyyy-MM-dd hh:mm:ss"],[Utils dateFromString:[NSDate date] byFormatter:@"yyyy-MM-dd hh:mm:ss"]] forKey:@"checkinout"];
        [workOrderDic setObject:workOrderObject.reason forKey:@"skipreason"];
        [workOrderDic setObject:workOrderObject.clientId forKey:@"client_id"];
        [workOrderDic setObject:[Utils dateFromString:workOrderObject.lastUpdate byFormatter:@"yyyy-MM-dd"] forKey:@"skip_resch_time"];
        [workOrderDic setObject:workOrderObject.orderNo forKey:@"order_no"];
        [workOrderDic setObject:userObject.userId forKey:@"cust_id"];
        [serverSendArray addObject:workOrderDic];
    
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
    NSData *json = [NSJSONSerialization dataWithJSONObject:serverSendArray
                                                   options:0
                                                     error:nil ];
    
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"workorder/dataSynByMobileApp/format/json?&time=%@&dbname=%@&key=%@&status=40&data=%@",todayDate,userObject.dbName,aServerKey,jsonString]andRequestType:@"GET"];
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil)
        {
            if([responeParseDic valueForKey:@"success"]!=Nil)
            {
                [workOrderObject setIsSync:[NSNumber numberWithInt:1]];
                WorkOrderManager * workOrderManager=[[WorkOrderManager alloc] init];
                
                [workOrderManager completeWorkOrder:workOrderObject];
            }
        }
    }

}
-(void)sendRescheduleVisitToServer:(WorkOrder *)workOrderObject
{
    
    
    //[dbname=cuztomise_childthief, key=JVP8xGk4hsX2cZd0L3NQwYbI0mf4exPiSoAhVYnz, status=30, data=[{"checkinout":",","mr_latlon_from":"0.0,0.0","visitDate":"2015-05-12","reschreason":"Doctor on emergency ","cust_id":"60","accompaniedby":"13,3","order_no":"app\/149\/4","id":"app\/149\/4","empid":"149","checkinlatlong":"0.0,0.0","startdate":"2015-05-12","client_id":"98","skip_resch_time":"2015-05-08 04:42:27"}]]
    
    NSData * returnData;
    LoginManager * loginManager=[[LoginManager alloc] init];
    User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    NSMutableArray * serverSendArray=[[NSMutableArray alloc] init];
        NSMutableDictionary * workOrderDic=[[NSMutableDictionary alloc] init];
        [workOrderDic setObject:workOrderObject.workOrderId forKey:@"id"];
        [workOrderDic setObject:userObject.userId forKey:@"emp_id"];
        [workOrderDic setObject:userObject.userId forKey:@"empid"];
        [workOrderDic setObject:[NSString stringWithFormat:@"%@,%@",[Utils dateFromString:workOrderObject.startDate byFormatter:@"yyyy-MM-dd hh:mm:ss"],[Utils dateFromString:[NSDate date] byFormatter:@"yyyy-MM-dd hh:mm:ss"]] forKey:@"checkinout"];
//        [workOrderDic setObject:[NSString stringWithFormat:@"%@,%@",workOrderObject.latitude,workOrderObject.longitude ] forKey:@"checkinout"];
        [workOrderDic setObject:[NSString stringWithFormat:@"%@,%@",workOrderObject.latitude,workOrderObject.longitude ] forKey:@"checkinlatlong"];
        
        [workOrderDic setObject:[NSString stringWithFormat:@"%@,%@",workOrderObject.latitude,workOrderObject.longitude ] forKey:@"mr_latlon_from"];
      [workOrderDic setObject:[Utils dateFromString:workOrderObject.startDate byFormatter:@"yyyy-MM-dd hh:mm:ss"] forKey:@"startdate"];
    [workOrderDic setObject:workOrderObject.reason forKey:@"reschreason"];
        [workOrderDic setObject:workOrderObject.notes forKey:@"extra_info"];
        [workOrderDic setObject:workOrderObject.clientId forKey:@"client_id"];
        [workOrderDic setObject:[Utils dateFromString:workOrderObject.lastUpdate byFormatter:@"yyyy-MM-dd"] forKey:@"skip_resch_time"];
        [workOrderDic setObject:[Utils dateFromString:workOrderObject.startDate byFormatter:@"yyyy-MM-dd hh:mm:ss"] forKey:@"visitDate"];
        [workOrderDic setObject:@"0" forKey:@"accompaniedby"];
        [workOrderDic setObject:workOrderObject.orderNo forKey:@"order_no"];
        [workOrderDic setObject:userObject.userId forKey:@"cust_id"];
        [serverSendArray addObject:workOrderDic];
    
    
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
    NSData *json = [NSJSONSerialization dataWithJSONObject:serverSendArray
                                                   options:0
                                                     error:nil ];
    
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"workorder/dataSynByMobileApp/format/json?&time=%@&dbname=%@&key=%@&status=30&data=%@",todayDate,userObject.dbName,aServerKey,jsonString]andRequestType:@"GET"];
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil)
        {
            if([responeParseDic valueForKey:@"success"]!=Nil)
            {
                [workOrderObject setIsSync:[NSNumber numberWithInt:1]];
                WorkOrderManager * workOrderManager=[[WorkOrderManager alloc] init];
                
                [workOrderManager completeWorkOrder:workOrderObject];
            }
        }
    }
    
}
-(void)addDoctorToServer
{
    //[{"birthdate":"","qualification":"","ppd":"","contact_no":"123456789","email":"","city":"indore","name":"jeera","zone":"dewas","zone_ids":"2"}]
    ClientManager * clientManager=[[ClientManager alloc] init];
    NSMutableArray * doctorArray=[clientManager getAllDoctorToSync];
    LoginManager * loginManager=[[LoginManager alloc] init];
    User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    for ( Client * clientObject in doctorArray) {
        NSData * returnData;
        NSMutableDictionary * dataDic=[[NSMutableDictionary alloc] init];
        [dataDic setObject:@"15-10-1988" forKey:@"birthdate"];
        [dataDic setObject:clientObject.qualification forKey:@"qualification"];
        [dataDic setValue:clientObject.contactNo forKey:@"contact_no"];
        [dataDic setObject:clientObject.email forKey:@"email"];
        [dataDic setObject:clientObject.city forKey:@"city"];
        [dataDic setObject:clientObject.name forKey:@"name"];
        NSString * zoneName=userObject.zones;
        NSArray * zoneArray=[zoneName componentsSeparatedByString:@","];
         NSArray * zoneId=[userObject.zoneId componentsSeparatedByString:@","];
        int zoneIdIndex=0;
        for (  NSString * zoneName in zoneArray) {
            if ([zoneName isEqualToString:clientObject.drZone]) {
                break;
            }
            zoneIdIndex=zoneIdIndex+1;
        }
        [dataDic setObject:[zoneId objectAtIndex:0] forKey:@"zone_ids"];
        [dataDic setObject:[zoneArray objectAtIndex:0] forKey:@"zone"];
        [dataDic setObject:clientObject.pPD forKey:@"ppd"];

        
        NSMutableArray * dataArray=[[NSMutableArray alloc] init];
        [dataArray addObject:dataDic];
        NSMutableData *postbody = [NSMutableData data];
        [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
        NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
        
        NSData *json = [NSJSONSerialization dataWithJSONObject:dataArray
                                                       options:0
                                                         error:nil ];
        
        NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        
        
        
        returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"client/addDoctorByApp/format/json?&time=%@&dbname=%@&key=%@&data=%@",todayDate,userObject.dbName,aServerKey,jsonString]andRequestType:@"GET"];
        if (returnData !=nil) {
            NSString *string = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            clientObject.clientId=[NSNumber numberWithInt:[string intValue]];
            [clientManager updateClient:clientObject];
        }
        NSLog(@"Rhohouhoip");
    }
  
}

@end
