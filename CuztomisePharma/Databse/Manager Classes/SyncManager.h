//
//  SyncManager.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 04/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseImportClass.h"

@interface SyncManager : NSObject
{
    AppDelegate * appDelegate;
}
-(void)firstTimeSync;
-(NSString *) syncServerToApp;
-(void)syncApptoServer;
-(WorkOrder *) getWorkOrderbyId:(NSString *)workOrderId;

@end
