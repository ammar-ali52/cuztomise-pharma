//
//  LoginManager.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 30/03/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "LoginManager.h"

@implementation LoginManager
-(NSString *)userLogin:(NSMutableDictionary *)loginDic{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    CLLocationManager * locaton=[appDelegate deviceLocation];
    NSData * returnData;
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"&pin=%@",[loginDic objectForKey:@"userPin"]] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"&deviceid=123456"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"&key=JVP8xGk4hsX2cZd0L3NQwYbI0mf4exPiSoAhVYnz"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"&lat=23.896987070"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"&long=-72.8799879y98"] dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];

    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"mobileapp/Loginapp/format/json?&time=%@&dbname=cuztomise_global&pin=%@&deviceid=%@&key=JVP8xGk4hsX2cZd0L3NQwYbI0mf4exPiSoAhVYnz&lat=%f&long=%f",todayDate,[loginDic objectForKey:@"userPin"],[Utils getValueFronUserDefault:aDeviceId],locaton.location.coordinate.latitude, locaton.location.coordinate.longitude]andRequestType:@"GET"];
    
//returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"mobileapp/Loginapp/format/json?&time=%@&dbname=cuztomise_global&pin=%@&deviceid=12345&key=JVP8xGk4hsX2cZd0L3NQwYbI0mf4exPiSoAhVYnz&lat=%f&long=%F",todayDate,[loginDic objectForKey:@"userPin"],locaton.location.coordinate.latitude, locaton.location.coordinate.longitude]andRequestType:@"GET"];
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil) {
            if ([[responeParseDic objectForKey:@"msg"]isEqualToString:aSuccess] && [[responeParseDic objectForKey:@"status"] isEqualToString:@"True"]) {
                NSMutableArray * resultArray=[responeParseDic objectForKey:@"results"];
                for (NSMutableDictionary * resultDic in resultArray) {
                    [self saveUser:resultDic];
                    [Utils setValueToUserDefault:[resultDic objectForKey:@"empid"] andKey:aUserId];
                    
                }
                return aSuccessfull;
            }
            else
            {
                return @"Please enter valid userpin";
            }
            
        }
        else
        {
            return @"Please enter valid userpin";
        }
    }
    else
    {
        return aInternetError;
        
    }
  
}
-(NSString *)userSignUp:(NSMutableDictionary *)signUpDic{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSData * returnData;
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"&pin=%@",[signUpDic objectForKey:@"userPin"]] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"&deviceid=12345"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"&key=JVP8xGk4hsX2cZd0L3NQwYbI0mf4exPiSoAhVYnz"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"&email=%@",[signUpDic objectForKey:@"emailId"]] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"&deviceos=iOS"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"&apiversion=1.0"] dataUsingEncoding:NSUTF8StringEncoding]];
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"mobileapp/signupmr/format/json?dbname=cuztomise_global&pin=%@&deviceid=%@&key=JVP8xGk4hsX2cZd0L3NQwYbI0mf4exPiSoAhVYnz&email=%@&deviceos=iOS&apiversion=1.0",[signUpDic objectForKey:@"userPin"],[Utils getValueFronUserDefault:aDeviceId],[signUpDic objectForKey:@"emailId"] ]andRequestType:@"GET"];
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil) {
            if ([[responeParseDic objectForKey:@"status"] isEqualToString:@"True"]) {
                return aSuccess;
                
            }
            else
            {
                return  aParsingError;
            }
            
        }
        else
        {
            return aParsingError;
        }
    }
    else
    {
        return aInternetError;
        
    }
    
}
-(NSString * )userForgotPassword:(NSMutableDictionary *)forgotDic{
    NSData * returnData;
    NSMutableData *postbody = [NSMutableData data];
    NSString * returnString;
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"mobileapp/Forgotpinemail/format/json?dbname=cuztomise_global&key=JVP8xGk4hsX2cZd0L3NQwYbI0mf4exPiSoAhVYnz&email=%@&vertical=Pharma",[forgotDic objectForKey:@"userEmail"] ]andRequestType:@"GET"];
    if (returnData !=nil) {
        NSError* error;
        NSMutableDictionary * responeParseDic = [NSJSONSerialization
                                                 JSONObjectWithData:returnData //1
                                                 options:kNilOptions
                                                 error:&error];
        if (error==nil && responeParseDic!=nil)
        {
            if ([[responeParseDic objectForKey:@"status"] isEqualToString:@"True"])
            {
                returnString= [responeParseDic objectForKey:aMsg];
            }
            else
            {
                returnString= [responeParseDic objectForKey:aMsg];

            }
        }
    }
    else
    {
        return aInternetError;
    }
    return returnString;

}
-(void)registerForPushNotification
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSData * returnData;
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&dbname=cuztomise_global"] dataUsingEncoding:NSUTF8StringEncoding]];
    User * userObject=[self getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"notification/insertPushNotiDetail/format/json?dbname=cuztomise_global&deviceid=%@&key=JVP8xGk4hsX2cZd0L3NQwYbI0mf4exPiSoAhVYnz&email=%@&devicetype=iphone&name=%@&regid=%@",[Utils getValueFronUserDefault:aDeviceId],userObject.emailId,userObject.empName,[Utils getValueFronUserDefault:aPushNotificationKey] ]andRequestType:@"GET"];
}
-(void)saveUser:(NSMutableDictionary *)userDic
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    User * userObject=[self getUserByUserId:[userDic objectForKey:@"empid"]];
    if (userObject==Nil) {
        userObject= (User *)[NSEntityDescription  insertNewObjectForEntityForName:@"User" inManagedObjectContext:appDelegate.managedObjectContext];
    }
    [userObject setEmpName:[userDic objectForKey:@"empname"]];
    [userObject setEmpId:[NSNumber numberWithInt:[[userDic objectForKey:@"empid"] intValue]]];
    [userObject setEmailId:[userDic objectForKey:@"emailid"]];
    [userObject setZones:[userDic objectForKey:@"zones"]];
    [userObject setZoneId:[userDic objectForKey:@"zones_id"]];
    [userObject setImagePath:[userDic objectForKey:@"image_path"]];
    [userObject setDbName:[userDic objectForKey:@"dbname"]];
    [userObject setCustId:[userDic objectForKey:@"cust_id"]];
    [userObject setCompanyName:[userDic objectForKey:@"company_name"]];
    [userObject setCompanyLogo:[userDic objectForKey:@"company_logo"]];
    [userObject setUserId:[NSNumber numberWithInt:[[userDic objectForKey:@"userid"] intValue]]];
    NSError *error = nil;
    
    if (![appDelegate.managedObjectContext save:&error])
    {
        NSLog(@"ERROR while saving UserList - %@", error);
    }
}
-(User * )getUserByUserId:(NSString *)userId{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User" inManagedObjectContext:appDelegate.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"empId = %@",userId]];
    [request setPredicate:predicate];
    [request setEntity:entity];
    
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    
    if (mutableFetchResults!=nil && [mutableFetchResults count]>0)
    {
        return [mutableFetchResults objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}
-(void)sendUserLocationtoServer
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    CLLocationManager * locaton=[appDelegate deviceLocation];
    NSData * returnData;
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"&lat=23.896987070"] dataUsingEncoding:NSUTF8StringEncoding]];
   
    User * userObject=[self getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
    NSMutableDictionary * dataDic=[[NSMutableDictionary alloc] init];
    [dataDic setObject:userObject.userId forKey:@"user_id"];
    [dataDic setObject:[NSString stringWithFormat:@"%f,%f",locaton.location.coordinate.latitude, locaton.location.coordinate.longitude] forKey:@"current_location"];
    [dataDic setObject:@"on" forKey:@"net_status"];
    [dataDic setObject:[Utils dateFromString:[NSDate date] byFormatter:@"yyyy-MM-dd"] forKey:@"net_status_timestamp"];
    [dataDic setObject:[Utils dateFromString:[NSDate date] byFormatter:@"yyyy-MM-dd"] forKey:@"gps_status_timestamp"];
    
    [dataDic setObject:@"on" forKey:@"gps_status"];
    [dataDic setObject:[NSString stringWithFormat:@"%f",locaton.location.coordinate.latitude] forKey:@"lat"];
    [dataDic setObject:[NSString stringWithFormat:@"%f",locaton.location.coordinate.longitude] forKey:@"long"];
    [dataDic setObject:[Utils dateFromString:[NSDate date] byFormatter:@"yyyy-MM-dd HH:mm:ss"] forKey:@"datetime"];
    NSMutableArray * dataArray=[[NSMutableArray alloc] init];
    [dataArray addObject:dataDic];
    NSData *json = [NSJSONSerialization dataWithJSONObject:dataArray
                                                   options:0
                                                     error:nil ];
    
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    
    
    NSString *todayDate=[Utils dateFromString:[NSDate date] byFormatter:@"HH:mm:ss"];
    returnData =[[ServerCalls getSingletonInstance] getServerResponseByMethod:postbody andAppendRequestURL:[NSString stringWithFormat: @"employee/createMrLogApp/format/json?&time=%@&dbname=%@&Is_Mobile=1&key=JVP8xGk4hsX2cZd0L3NQwYbI0mf4exPiSoAhVYnz&data=%@",todayDate,userObject.dbName,jsonString]andRequestType:@"GET"];
    
 
}
@end
