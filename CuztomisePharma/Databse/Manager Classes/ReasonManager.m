//
//  ReasonManager.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 12/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "ReasonManager.h"

@implementation ReasonManager
-(NSMutableArray *) getAllReason
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Reason" inManagedObjectContext:appDelegate.managedObjectContext];
    [request setEntity:entity];
    NSError *error = nil;
    NSMutableArray *mutableFetchResults =(NSMutableArray*)[appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    if (mutableFetchResults == nil)
    {
        NSLog(@"ERROR - %@", error);
    }
    return mutableFetchResults;
}
@end
