//
//  Client.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 28/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "Client.h"


@implementation Client

@dynamic alias;
@dynamic authCode;
@dynamic avgTime;
@dynamic categoryId;
@dynamic city;
@dynamic clientId;
@dynamic clientNo;
@dynamic companyName;
@dynamic contactNo;
@dynamic dob;
@dynamic doctorCode;
@dynamic drugPrescribed;
@dynamic drugPromoted;
@dynamic drugSuggest;
@dynamic drZone;
@dynamic duration;
@dynamic email;
@dynamic employeeAssign;
@dynamic id;
@dynamic imagePath;
@dynamic isActive;
@dynamic isSync;
@dynamic name;
@dynamic notes;
@dynamic pPD;
@dynamic prefDay;
@dynamic prefTime;
@dynamic qualification;
@dynamic speciality;
@dynamic state;
@dynamic userId;
@dynamic zoneId;

@end
