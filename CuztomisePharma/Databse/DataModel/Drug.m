//
//  Drug.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 05/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "Drug.h"


@implementation Drug

@dynamic drugId;
@dynamic drugName;
@dynamic drugCode;
@dynamic dateAdded;
@dynamic addedBy;
@dynamic divisionId;

@end
