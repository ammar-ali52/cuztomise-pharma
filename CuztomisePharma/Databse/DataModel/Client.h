//
//  Client.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 28/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Client : NSManagedObject

@property (nonatomic, retain) NSString * alias;
@property (nonatomic, retain) NSString * authCode;
@property (nonatomic, retain) NSString * avgTime;
@property (nonatomic, retain) NSString * categoryId;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSNumber * clientId;
@property (nonatomic, retain) NSNumber * clientNo;
@property (nonatomic, retain) NSString * companyName;
@property (nonatomic, retain) NSString * contactNo;
@property (nonatomic, retain) NSDate * dob;
@property (nonatomic, retain) NSString * doctorCode;
@property (nonatomic, retain) NSString * drugPrescribed;
@property (nonatomic, retain) NSString * drugPromoted;
@property (nonatomic, retain) NSString * drugSuggest;
@property (nonatomic, retain) NSString * drZone;
@property (nonatomic, retain) NSString * duration;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * employeeAssign;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * imagePath;
@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSNumber * isSync;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * pPD;
@property (nonatomic, retain) NSString * prefDay;
@property (nonatomic, retain) NSString * prefTime;
@property (nonatomic, retain) NSString * qualification;
@property (nonatomic, retain) NSString * speciality;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSString * zoneId;

@end
