//
//  LastSync.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 05/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface LastSync : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSDate * lastSync;

@end
