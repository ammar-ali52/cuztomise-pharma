//
//  User.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 05/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject

@property (nonatomic, retain) NSString * companyLogo;
@property (nonatomic, retain) NSString * companyName;
@property (nonatomic, retain) NSString * custId;
@property (nonatomic, retain) NSString * dbName;
@property (nonatomic, retain) NSString * emailId;
@property (nonatomic, retain) NSNumber * empId;
@property (nonatomic, retain) NSString * empName;
@property (nonatomic, retain) NSString * imagePath;
@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSString * zones;
@property (nonatomic, retain) NSString * zoneId;

@end
