//
//  LocationPing.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 05/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "LocationPing.h"


@implementation LocationPing

@dynamic id;
@dynamic isSync;
@dynamic latitude;
@dynamic longitude;
@dynamic status;
@dynamic timeStamp;

@end
