//
//  WorkOrder.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 09/05/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface WorkOrder : NSManagedObject

@property (nonatomic, retain) NSDate * avgTimeWork;
@property (nonatomic, retain) NSString * billingType;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSNumber * clientAddId;
@property (nonatomic, retain) NSNumber * clientId;
@property (nonatomic, retain) NSString * clientNo;
@property (nonatomic, retain) NSString * complexity;
@property (nonatomic, retain) NSString * contactNo;
@property (nonatomic, retain) NSString * cpEmail;
@property (nonatomic, retain) NSString * cpName;
@property (nonatomic, retain) NSString * cpPhone;
@property (nonatomic, retain) NSNumber * createdBy;
@property (nonatomic, retain) NSDate * dateApproved;
@property (nonatomic, retain) NSDate * dateCreated;
@property (nonatomic, retain) NSString * detail;
@property (nonatomic, retain) NSNumber * discount;
@property (nonatomic, retain) NSString * drugPrescribed;
@property (nonatomic, retain) NSString * drugPromoted;
@property (nonatomic, retain) NSString * drugSuggest;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSDate * endDate;
@property (nonatomic, retain) NSString * estimatedWorkTime;
@property (nonatomic, retain) NSString * extraInfo;
@property (nonatomic, retain) NSString * feedback;
@property (nonatomic, retain) NSNumber * grandTotal;
@property (nonatomic, retain) NSNumber * ifVisits;
@property (nonatomic, retain) NSString * invoicing;
@property (nonatomic, retain) NSNumber * isAdminNotify;
@property (nonatomic, retain) NSNumber * isComplete;
@property (nonatomic, retain) NSNumber * isEmpNotify;
@property (nonatomic, retain) NSNumber * isHidden;
@property (nonatomic, retain) NSNumber * isInvoiced;
@property (nonatomic, retain) NSNumber * isMonthVisit;
@property (nonatomic, retain) NSNumber * isNotifySent;
@property (nonatomic, retain) NSNumber * isReschedule;
@property (nonatomic, retain) NSNumber * isRescheduleTimes;
@property (nonatomic, retain) NSNumber * isSync;
@property (nonatomic, retain) NSString * jobType;
@property (nonatomic, retain) NSString * lastDuaration;
@property (nonatomic, retain) NSNumber * lastsValue;
@property (nonatomic, retain) NSDate * lastUpdate;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * mrLatLongFrom;
@property (nonatomic, retain) NSString * mrLocationFrom;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * notificationEmail;
@property (nonatomic, retain) NSString * orderNo;
@property (nonatomic, retain) NSDate * prefStartTime;
@property (nonatomic, retain) NSString * priority;
@property (nonatomic, retain) NSNumber * projectLimitAmt;
@property (nonatomic, retain) NSString * qualification;
@property (nonatomic, retain) NSString * reason;
@property (nonatomic, retain) NSDate * receiveDate;
@property (nonatomic, retain) NSNumber * scheduleIds;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSNumber * total;
@property (nonatomic, retain) NSString * woExtraField;
@property (nonatomic, retain) NSDate * workDateTimeCreated;
@property (nonatomic, retain) NSNumber * workOrderId;
@property (nonatomic, retain) NSString * zoneId;
@property (nonatomic, retain) NSString * zoneName;

@end
