//
//  Drug.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 05/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Drug : NSManagedObject

@property (nonatomic, retain) NSNumber * drugId;
@property (nonatomic, retain) NSString * drugName;
@property (nonatomic, retain) NSString * drugCode;
@property (nonatomic, retain) NSDate * dateAdded;
@property (nonatomic, retain) NSNumber * addedBy;
@property (nonatomic, retain) NSNumber * divisionId;

@end
