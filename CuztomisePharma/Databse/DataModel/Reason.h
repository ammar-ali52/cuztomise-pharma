//
//  Reason.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 05/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Reason : NSManagedObject

@property (nonatomic, retain) NSNumber * reasonId;
@property (nonatomic, retain) NSString * reason;
@property (nonatomic, retain) NSNumber * addedBy;
@property (nonatomic, retain) NSDate * dateCreated;

@end
