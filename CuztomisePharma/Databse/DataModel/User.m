//
//  User.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 05/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic companyLogo;
@dynamic companyName;
@dynamic custId;
@dynamic dbName;
@dynamic emailId;
@dynamic empId;
@dynamic empName;
@dynamic imagePath;
@dynamic userId;
@dynamic zones;
@dynamic zoneId;

@end
