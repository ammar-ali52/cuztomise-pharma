//
//  ChangeLog.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 10/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "ChangeLog.h"


@implementation ChangeLog

@dynamic action;
@dynamic changeId;
@dynamic isSync;
@dynamic workId;
@dynamic userId;
@dynamic timestamp;
@dynamic changeCode;
@dynamic isAck;

@end
