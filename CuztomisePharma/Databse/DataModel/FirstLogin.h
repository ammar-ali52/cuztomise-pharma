//
//  FirstLogin.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 05/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FirstLogin : NSManagedObject

@property (nonatomic, retain) NSString * firstLogin;
@property (nonatomic, retain) NSNumber * id;

@end
