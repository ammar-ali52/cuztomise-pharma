//
//  Reason.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 05/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "Reason.h"


@implementation Reason

@dynamic reasonId;
@dynamic reason;
@dynamic addedBy;
@dynamic dateCreated;

@end
