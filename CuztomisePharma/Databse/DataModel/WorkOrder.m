//
//  WorkOrder.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 09/05/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import "WorkOrder.h"


@implementation WorkOrder

@dynamic avgTimeWork;
@dynamic billingType;
@dynamic city;
@dynamic clientAddId;
@dynamic clientId;
@dynamic clientNo;
@dynamic complexity;
@dynamic contactNo;
@dynamic cpEmail;
@dynamic cpName;
@dynamic cpPhone;
@dynamic createdBy;
@dynamic dateApproved;
@dynamic dateCreated;
@dynamic detail;
@dynamic discount;
@dynamic drugPrescribed;
@dynamic drugPromoted;
@dynamic drugSuggest;
@dynamic email;
@dynamic endDate;
@dynamic estimatedWorkTime;
@dynamic extraInfo;
@dynamic feedback;
@dynamic grandTotal;
@dynamic ifVisits;
@dynamic invoicing;
@dynamic isAdminNotify;
@dynamic isComplete;
@dynamic isEmpNotify;
@dynamic isHidden;
@dynamic isInvoiced;
@dynamic isMonthVisit;
@dynamic isNotifySent;
@dynamic isReschedule;
@dynamic isRescheduleTimes;
@dynamic isSync;
@dynamic jobType;
@dynamic lastDuaration;
@dynamic lastsValue;
@dynamic lastUpdate;
@dynamic latitude;
@dynamic location;
@dynamic longitude;
@dynamic mrLatLongFrom;
@dynamic mrLocationFrom;
@dynamic name;
@dynamic notes;
@dynamic notificationEmail;
@dynamic orderNo;
@dynamic prefStartTime;
@dynamic priority;
@dynamic projectLimitAmt;
@dynamic qualification;
@dynamic reason;
@dynamic receiveDate;
@dynamic scheduleIds;
@dynamic startDate;
@dynamic status;
@dynamic subject;
@dynamic total;
@dynamic woExtraField;
@dynamic workDateTimeCreated;
@dynamic workOrderId;
@dynamic zoneId;
@dynamic zoneName;

@end
