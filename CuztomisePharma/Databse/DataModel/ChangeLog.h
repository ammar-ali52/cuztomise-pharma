//
//  ChangeLog.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 10/04/15.
//  Copyright (c) 2015 Ammar Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ChangeLog : NSManagedObject

@property (nonatomic, retain) NSString * action;
@property (nonatomic, retain) NSNumber * changeId;
@property (nonatomic, retain) NSNumber * isSync;
@property (nonatomic, retain) NSNumber * workId;
@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSDate * timestamp;
@property (nonatomic, retain) NSString * changeCode;
@property (nonatomic, retain) NSNumber * isAck;

@end
