//
//  RootTableViewCell.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 23/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import "RootTableViewCell.h"

@implementation RootTableViewCell

- (void)awakeFromNib {
    [_nameLabel setTextColor:[UIColor appGrayColor]];
    [_nameLabel setFont:[UIFont appNormalFont]];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
