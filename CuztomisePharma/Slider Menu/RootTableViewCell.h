//
//  RootTableViewCell.h
//  CuztomisePharma
//
//  Created by Ammar Ali on 23/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"

@interface RootTableViewCell : UITableViewCell
{
    
    
    
}
@property (nonatomic,retain)IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *countLabel;
@property (nonatomic,retain)IBOutlet UIImageView *labelImage;
@end
