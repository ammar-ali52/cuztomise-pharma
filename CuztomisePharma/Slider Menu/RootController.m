 //
//  RootController.m
//  SlideMenu
//
//  Created by 6DegreesIT on 1/23/15.
//  Copyright (c) 2015 6Degreesit. All rights reserved.
//

#define kExposedWidth -200.0
#define kMenuCellID @"MenuCell"

#import "RootController.h"
dispatch_source_t CreateDispatchTimer(double interval, dispatch_queue_t queue, dispatch_block_t block)
{
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    if (timer)
    {
        dispatch_source_set_timer(timer, dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), interval * NSEC_PER_SEC, (1ull * NSEC_PER_SEC) / 10);
        dispatch_source_set_event_handler(timer, block);
        dispatch_resume(timer);
    }
    return timer;
}
@interface RootController()

@property (nonatomic, strong) UITableView *menu;
@property (nonatomic, strong) NSArray *viewControllers;
@property (nonatomic, strong) NSArray *menuTitles;

@property (nonatomic, assign) NSInteger indexOfVisibleController;

@property (nonatomic, assign) BOOL isMenuVisible;

@end


@implementation RootController

- (id)initWithViewControllers:(NSArray *)viewControllers andMenuTitles:(NSArray *)titles andCurrentPage:(int)pageshow{
    if (self = [super init])
    {
        currentPage=pageshow;
        NSAssert(self.viewControllers.count == self.menuTitles.count, @"There must be one and only one menu title corresponding to every view controller!");    // (1)
        NSMutableArray *tempVCs = [NSMutableArray arrayWithCapacity:viewControllers.count];
        imageNameArray =[[NSMutableArray alloc] initWithObjects:@"home.png",@"visit.png",@"doctor.png",@"calendar.png",@"wall.png",@"setting.png",@"sales.png",@"admin.png",@"logout.png",@"visit.png",@"visit.png", nil];
        self.menuTitles = [titles copy];
        
        for (UIViewController *vc in viewControllers) // (2)
        {
            if (![vc isMemberOfClass:[UINavigationController class]])
            {
                [tempVCs addObject:[[UINavigationController alloc] initWithRootViewController:vc]];
            }
            else
                [tempVCs addObject:vc];
            
            
            UIView* viewContainer = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 35, 50)];
            UIButton* menuBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            menuBtn.frame = CGRectMake(5, 10, 35, 28);
            [menuBtn setImage:[[UIImage imageNamed:@"drawer.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
            [viewContainer addSubview:menuBtn];
            [menuBtn addTarget:self action:@selector(toggleMenuVisibility:) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem* barBtnItem = [[UIBarButtonItem alloc] initWithCustomView:viewContainer];
           
            UIButton * iconBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
            [iconBtn setImage:[UIImage imageNamed:@"icon.png"] forState:UIControlStateNormal];
            [iconBtn setFrame:CGRectMake(240, 10,29, 30)];
            UIBarButtonItem *iconBarButton=[[UIBarButtonItem alloc]initWithCustomView:iconBtn];
            
            UIViewController *topVC = ((UINavigationController *)tempVCs.lastObject).topViewController;
            topVC.navigationItem.rightBarButtonItems = [@[barBtnItem,iconBarButton] arrayByAddingObjectsFromArray:topVC.navigationItem.rightBarButtonItems];
            
//            
//            if ([[UIDevice currentDevice].systemVersion floatValue] > 6.1)
//            {
//                topVC.navigationController.navigationBar.tintColor = [UIColor navigationTitleLblColor];
//                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
//                [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary    dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"PTSans-Regular" size:20], NSFontAttributeName, nil]];
//            }
//            else
//            {
//                self.navigationController.navigationBar.barTintColor = [UIColor navigationBarBGColor];
//                [[UINavigationBar appearance] setTitleTextAttributes: @{
//                                                                        NSForegroundColorAttributeName: [UIColor navigationTitleLblColor],
//                                                                        NSFontAttributeName: [UIFont normalFontSize]
//                                                                        }];
//            }
            topVC.navigationController.navigationBar.barTintColor = [UIColor appBlackColor];

          
            
        }
        self.viewControllers = [tempVCs copy];
        self.menu = [[UITableView alloc] init]; // (4)
        self.menu.delegate = self;
        self.menu.dataSource = self;
        
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Root";
    [self.menu registerClass:[UITableViewCell class] forCellReuseIdentifier:kMenuCellID];
    self.view.backgroundColor = [UIColor appGrayColor];
    self.menu.frame = CGRectMake(0, 65, self.view.bounds.size.width, self.view.bounds.size.height-65);
    self.menu.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.menu];
    
    self.indexOfVisibleController = currentPage;
    UIViewController *visibleViewController = self.viewControllers[currentPage];
    visibleViewController.view.frame = [self offScreenFrame];
    [self addChildViewController:visibleViewController]; // (5)
    [self.view addSubview:visibleViewController.view]; // (6)
    self.isMenuVisible = NO;
    [self adjustContentFrameAccordingToMenuVisibility]; // (7)
        
    [self.viewControllers[currentPage] didMoveToParentViewController:self]; // (8)
    
}

- (void)toggleMenuVisibility:(id)sender // (9)
{
    self.isMenuVisible = !self.isMenuVisible;
    [self adjustContentFrameAccordingToMenuVisibility];
}



- (void)adjustContentFrameAccordingToMenuVisibility // (10)
{
    UIViewController *visibleViewController = self.viewControllers[self.indexOfVisibleController];
    CGSize size = visibleViewController.view.frame.size;
    
    if (self.isMenuVisible)
    {
        [UIView animateWithDuration:0.3 animations:^{
            visibleViewController.view.frame = CGRectMake(kExposedWidth, 0, size.width, size.height);
        }];
    }
    else
        [UIView animateWithDuration:0.3 animations:^{
            visibleViewController.view.frame = CGRectMake(0, 0, size.width, size.height);
        }];
    
}

- (void)replaceVisibleViewControllerWithViewControllerAtIndex:(NSInteger)index // (11)
{
    if (index == self.indexOfVisibleController)
    {
        [self toggleMenuVisibility:Nil];
        return;
    }
    UIViewController *incomingViewController = self.viewControllers[index];
    incomingViewController.view.frame = [self offScreenFrame];
    UIViewController *outgoingViewController = self.viewControllers[self.indexOfVisibleController];
    CGRect visibleFrame = self.view.bounds;
    
    
    [outgoingViewController willMoveToParentViewController:nil]; // (12)
    
    [self addChildViewController:incomingViewController]; // (13)
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents]; // (14)
    [self transitionFromViewController:outgoingViewController // (15)
                      toViewController:incomingViewController
                              duration:0.0 options:0
                            animations:^{
                                outgoingViewController.view.frame = [self offScreenFrame];
                                
                            }
     
                            completion:^(BOOL finished) {
                                [UIView animateWithDuration:0.0
                                                 animations:^{
                                                     [outgoingViewController.view removeFromSuperview];
                                                     [self.view addSubview:incomingViewController.view];
                                                     incomingViewController.view.frame = visibleFrame;
                                                     [[UIApplication sharedApplication] endIgnoringInteractionEvents]; // (16)
                                                 }];
                                [incomingViewController didMoveToParentViewController:self]; // (17)
                                [outgoingViewController removeFromParentViewController]; // (18)
                                self.isMenuVisible = NO;
                                self.indexOfVisibleController = index;
                            }];
    self.navigationController.navigationBar.barTintColor = [UIColor appBlackColor];
}


// (19):
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
        return 80;
    else
        return 60;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuTitles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        static NSString *simpleTableIdentifier = @"RootTableViewUserCell";
        
        RootTableViewUserCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            
            NSArray * nibArray=[[NSBundle mainBundle] loadNibNamed:@"RootTableViewUserCell" owner:self options:nil];
            cell = [nibArray objectAtIndex:0];
        }
        LoginManager * loginManager=[[LoginManager alloc] init];
        User * userObject=[loginManager getUserByUserId:[Utils getValueFronUserDefault:aUserId]];
        cell.userNameTextField.text=userObject.empName;
        loginManager=nil;
               
        return cell;
    }
    else
    {
       
        static NSString *simpleTableIdentifier = @"RootTableViewCell";
        
        RootTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            
            NSArray * nibArray=[[NSBundle mainBundle] loadNibNamed:@"RootTableViewCell" owner:self options:nil];
            cell = [nibArray objectAtIndex:0];
        }
        if (indexPath.row==2) {
            WorkOrderManager * workOrderManager=[[WorkOrderManager alloc] init];
            NSMutableArray * workOrderArray=[workOrderManager getAllWorkOrder];
            cell.countLabel.text=[NSString stringWithFormat:@"%lu",(unsigned long)[workOrderArray count] ];
        }
        else
        {
            [cell.countLabel setHidden:YES];
        }
        cell.nameLabel.text=self.menuTitles[indexPath.row];
        cell.labelImage.image=[UIImage imageNamed:[imageNameArray objectAtIndex:indexPath.row-1]];
        //    cell.textLabel.text = self.menuTitles[indexPath.row];
        
        return cell;
    }
   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row == self.menuTitles.count-1)
//    {
//        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you really want to Signout" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
//        alert.tag = 1;
//        [alert show];
//    }
//    else
//    {
//        [self replaceVisibleViewControllerWithViewControllerAtIndex:indexPath.row];
//    }
    NSLog(@"%ld",(long)indexPath.row);
    if (indexPath.row-1==-1) {
        [self replaceVisibleViewControllerWithViewControllerAtIndex:0];
    }
    else if(indexPath.row==6)
    {
        if ([self checkInternet]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StartActivityIndicator" object:nil];
            
            
            syncAlertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you really want to Sync" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            syncAlertView.tag = 1;
            [syncAlertView show];
        }
        else
        {
            syncAlertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:aInternetError delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            syncAlertView.tag = 1;
            [syncAlertView show];
            [self toggleMenuVisibility:Nil];
        }
//        [self replaceVisibleViewControllerWithViewControllerAtIndex:0];
       
    }
    else if (indexPath.row==8) {
        syncAlertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you really want to Logout" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        syncAlertView.tag = 2;
        [syncAlertView show];
    }
    else{
        [self replaceVisibleViewControllerWithViewControllerAtIndex:indexPath.row-1];

    }
}

- (CGRect)offScreenFrame
{
    return CGRectMake(self.view.bounds.size.width, 0, self.view.bounds.size.width, self.view.bounds.size.height);
}

#pragma mark -
#pragma mark Alert Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)
    {
        if (buttonIndex == 0)
        {
            [self toggleMenuVisibility:Nil];
        }
        else
        {
            [self toggleMenuVisibility:Nil];
//            [self startTimer];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self syncServerCall];
            });
//
            
          
            
//            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//            [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(syncServerCall) userInfo:nil repeats:false];
//            [self replaceVisibleViewControllerWithViewControllerAtIndex:0];

        }
        
    }
    if(alertView.tag == 2)
    {
        if (buttonIndex == 0)
        {
            [self toggleMenuVisibility:Nil];
        }
        else
        {
            LoginViewController * loginView=[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:loginView animated:YES];
//            [self replaceVisibleViewControllerWithViewControllerAtIndex:self.menuTitles.count-1];
        }
        
    }
}
-(void)syncServerCall
{
    SyncManager * syncManager=[[SyncManager alloc] init];
    NSString * responseString=[syncManager syncServerToApp];
    syncManager=nil;
    if (![responseString isEqualToString:aSuccessfull]) {
        syncAlertView = [[UIAlertView alloc]initWithTitle:@"Error" message:aInternetError delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        syncAlertView.tag = 10;
        [syncAlertView show];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopActivityIndicator" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateDashboardCell" object:nil];
    

}
- (void)startTimer
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    double secondsToFire = 30*60*1.000f;
    
    _timer = CreateDispatchTimer(secondsToFire, queue, ^{
        SyncManager * syncManager=[[SyncManager alloc] init];
        [syncManager syncServerToApp];
        syncManager=nil;
    });
}
- (BOOL) connectedToNetwork
{
    Reachability *r = [Reachability reachabilityForInternetConnection];//[Reachability reachabilityWithHostName:@"kumoteamdemo.pagodabox.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    BOOL internet;
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
    {
        internet = NO;
    } else {
        internet = YES;
    }
    return internet;
}

-(BOOL) checkInternet
{
    //Make sure we have internet connectivity
    if([self connectedToNetwork] != YES)
    {
        return NO;
    }
    else {
        return YES;
    }
}


@end
