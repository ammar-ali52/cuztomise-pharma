//
//  RootController.h
//  SlideMenu
//
//  Created by 6DegreesIT on 1/23/15.
//  Copyright (c) 2015 6Degreesit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImportClass.h"


@interface RootController : UIViewController<UITableViewDataSource, UITableViewDelegate,UIAlertViewDelegate>
{
    NSMutableArray * imageNameArray;
    int currentPage;
    UIAlertView * syncAlertView;
    dispatch_source_t _timer;
}

- (id)initWithViewControllers:(NSArray *)viewControllers andMenuTitles:(NSArray *)titles andCurrentPage:(int)pageshow; // (2)

@end
