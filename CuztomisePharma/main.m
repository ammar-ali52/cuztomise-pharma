//
//  main.m
//  CuztomisePharma
//
//  Created by Ammar Ali on 16/03/15.
//  Copyright (c) 2015 Incoders.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
